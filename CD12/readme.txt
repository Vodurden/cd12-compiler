Compiling:
	This need to be compiled with the -std=c++11 flag
	under g++. The relevant folders are output,
	parser, scanner, symboltable and main.cpp. Please
	don't include the test folder as it defines another
	main and requires boost_unit_test_framework

To execute the program, ensure that the html_output is in the same
location as the path you are executing from. I.e if the CD12
executable is located in CD12/Debug and html_output is located
in CD12/html_output then the correct command would be:

	cd CD12
	Debug/CD12 < input.txt

If the university computers supplied boost this would not
be neccecary but unfortunately it is. Any errors
will be reported on the standard output and the parse
tree is generated and placed under html_output/syntax_tree.html

Please ensure that javascript is enabled for syntax_tree.html
