#include "ScopeNode.h"

using namespace cd12;
using namespace symboltable;

ScopeNode::ScopeNode(ScopeNode* parent) :
	m_parent(parent) {
}

ScopeNode* ScopeNode::parent() {
	return m_parent;
}

ScopeNode* ScopeNode::child(const std::string& name) {
	// Try and find the child, if it doesn't exist return a nullptr
	if(hasChild(name)) {
		return m_children[name];
	}

	return 0;
}

bool ScopeNode::hasChild(const std::string& name) {
	return (m_children.find(name) != m_children.end());
}


symboltable::SymbolTable& ScopeNode::table() {
	return m_table;
}

ScopeNode* ScopeNode::addChild(const std::string& name, ScopeNode* node) {
	m_children[name] = node;
	return node;
}
