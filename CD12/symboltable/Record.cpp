/*
 * Record.cpp
 *
 *  Created on: Aug 8, 2012
 *      Author: vodurden
 */

#include "Record.h"

using namespace cd12;
using namespace symboltable;

Record::Record(std::string id) :
		m_id(id),
		m_type(TYPE_UNDEFINED),
		m_register(-1),
		m_memOffset(-1) {
}

std::string Record::getID() {
	return m_id;
}

Type Record::getType() {
	return m_type;
}

void Record::setType(Type type) {
	m_type = type;
}

void Record::merge(Record* other) {
	if(this->m_type == TYPE_UNDEFINED) {
		this->m_type = other->m_type;
	}
}

int Record::memOffset() {
	return m_memOffset;
}

void Record::memOffset(int offset) {
	m_memOffset = offset;
}

int Record::baseRegister() {
	return m_register;
}

void Record::baseRegister(int reg) {
	m_register = reg;
}
