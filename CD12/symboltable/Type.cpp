#include "Type.h"

std::string cd12::symboltable::toString(cd12::symboltable::Type type) {
	const char* types[] = {
		"UNDEFINED", "FLOAT", "ARRAY_BOUNDED",
		"ARRAY_UNBOUNDED", "FUNCTION", "PROCEDURE",
		"REF_PARAM"
	};

	return std::string(types[type]);
}
