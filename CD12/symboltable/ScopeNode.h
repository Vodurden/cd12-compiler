#ifndef CD12_SYMBOLTABLE_SCOPENODE_H_
#define CD12_SYMBOLTABLE_SCOPENODE_H_
#include <map>
#include <string>
#include "SymbolTable.h"

namespace cd12 {
namespace symboltable {

	class ScopeNode {
		public:
			ScopeNode(ScopeNode* parent);

			SymbolTable& table();

			ScopeNode* parent();

			ScopeNode* child(const std::string& name);
			ScopeNode* addChild(const std::string& name, ScopeNode* node);
			bool hasChild(const std::string& name);
		private:
			SymbolTable m_table;
			ScopeNode* m_parent;
			std::map<std::string, ScopeNode*> m_children;
	};

}
}
#endif
