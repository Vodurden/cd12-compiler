#ifndef CD12_SYMBOLTABLE_SYMBOLTABLEMANAGER_H 
#define CD12_SYMBOLTABLE_SYMBOLTABLEMANAGER_H
#include <map>
#include <string>
#include "Record.h"
#include "ScopeNode.h"
#include "../scanner/TokenID.h"
namespace cd12 {
	namespace symboltable {

		class SymbolTableManager {
			public:
				SymbolTableManager();
				~SymbolTableManager();

				// Move down a scope
				void pushScope(const std::string& name);

				// Move out of the current scope
				void popScope();

				// Finds the record corresponding to the given name,
				// if no such record is found then it returns nullptr
				Record* lookup(const std::string& name);

				// Adds a record to the current scoped symbol table
				// if the record already exists the records are merged
				// with preference to the existing record
				Record* addRecord(Record* record);

				Record* addConstant(Record* record);

			private:
				ScopeNode* m_root;
				ScopeNode* m_currentScope;
		};

	} 
}
#endif 
