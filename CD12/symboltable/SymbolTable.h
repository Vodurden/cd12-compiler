#ifndef CD12_SYMBOLTABLE_SYMBOLTABLE_H
#define CD12_SYMBOLTABLE_SYMBOLTABLE_H
#include <map>
#include <string>
#include "Record.h"
namespace cd12 {
	namespace symboltable {
		class SymbolTable {
			public:
				// Adds the record, if the record already exists
				// does nothing
				void addRecord(const std::string& id, Record* data);

				Record* getRecord(const std::string& id);

				// Returns true if a symbol is defined.
				bool isDefined(const std::string& id);
			private:
				std::map<std::string, Record*> m_records;
		};
	}
}

inline void cd12::symboltable::SymbolTable::addRecord(const std::string& id,
		Record* data) {
	if (!isDefined(id)) {
		m_records[id] = data;
	}
}

inline cd12::symboltable::Record* cd12::symboltable::SymbolTable::getRecord(
		const std::string& id) {
	return m_records[id];
}

inline bool cd12::symboltable::SymbolTable::isDefined(
		const std::string& id) {
	return !(m_records.find(id) == m_records.end());
}

#endif
