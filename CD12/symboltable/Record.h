#ifndef CD12_SYMBOLTABLE_RECORD_H
#define CD12_SYMBOLTABLE_RECORD_H
#include <string>
#include "Type.h"

namespace cd12 {
	namespace symboltable {

		class Record {
			public:
				Record(std::string id);

				std::string getID();

				Type getType();
				void setType(Type type);

				// Merge the data of the other record into this one
				void merge(Record* other);

				int baseRegister();
				void baseRegister(int reg);

				int memOffset();
				void memOffset(int offset);
			private:
				// Stuff we can work out from the lexer, for numbers it's the number,
				// for ids it's the id name
				std::string m_id;

				// Data we assign in the parser
				Type m_type;

				// Data we assign in the code generation
				int m_register;
				int m_memOffset;
		};

	} /* namespace symboltable */
} /* namespace cd12 */
#endif /* CD12_SYMBOLTABLE_RECORD_H */
