/*
 * SymbolTableManager.cpp
 *
 *  Created on: Aug 10, 2012
 *      Author: vodurden
 */

#include "SymbolTableManager.h"

using namespace cd12;
using namespace symboltable;

cd12::symboltable::SymbolTableManager::SymbolTableManager() {
	m_root = new ScopeNode(0);
	m_currentScope = m_root;
}

SymbolTableManager::~SymbolTableManager() {
	// TODO: Make delete m_root chain or recurse here
	// delete root = SEGFAULT, TODO: INVESTIGATE
}

void SymbolTableManager::pushScope(const std::string& name) {
	// Check if the scope already exists, if so move to it
	if(m_currentScope->hasChild(name)) {
		m_currentScope = m_currentScope->child(name);
	} else {
		// Create a new child with the current scope as it's parent
		ScopeNode* child = new ScopeNode(m_currentScope);
		m_currentScope = m_currentScope->addChild(name, child);
	}
}

void SymbolTableManager::popScope() {
	// If we are the root ignore, otherwise move up a scope
	if(m_currentScope == m_root) { return; }

	m_currentScope = m_currentScope->parent();
}

Record* SymbolTableManager::lookup(const std::string& name) {
	// Try and find the record by traversing up the tree. starting with current
	ScopeNode* search = m_currentScope;
	while(search != 0) {
		if(m_currentScope->table().isDefined(name)) {
			// We found it!
			return m_currentScope->table().getRecord(name);
		} else {
			search = search->parent();
		}
	}

	return 0;
}

Record* SymbolTableManager::addRecord(Record* record) {
	if(m_currentScope->table().isDefined(record->getID())) {
		// Merge the records and return the existing record
		Record* existing = m_currentScope->table().getRecord(record->getID());
		record->merge(existing);
		return existing;
	} else {
		m_currentScope->table().addRecord(record->getID(), record);
		return record;
	}
}


Record* SymbolTableManager::addConstant(Record* record) {
	if(m_root->table().isDefined(record->getID())) {
		return m_root->table().getRecord(record->getID());
	} else {
		m_root->table().addRecord(record->getID(), record);
		return record;
	}
}
