#ifndef CD12_SYMBOLTABE_TYPE_H
#define CD12_SYMBOLTABE_TYPE_H
#include <string>
namespace cd12 {
	namespace symboltable {
		enum Type {
			TYPE_ENUM_START = 0,
			TYPE_UNDEFINED = 0,
			TYPE_FLOAT,
			TYPE_ARRAY_BOUNDED, // arrayName[size]
			TYPE_ARRAY_UNBOUNDED, // arrayName[]
			TYPE_FUNCTION,
			TYPE_PROCEDURE,
			TYPE_REF_PARAM,
			TYPE_ENUM_END
		};

		std::string toString(Type type);
	}
}
#endif
