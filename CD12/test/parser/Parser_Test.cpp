#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#   define BOOST_TEST_MODULE Main
#endif
#include <algorithm>
#include <fstream>
#include <sstream>
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "../../scanner/Scanner.h"
#include "../../scanner/utility/Utility.h"
#include "../../symboltable/SymbolTableManager.h"
#include "../../parser/TokenExaminer.h"
#include "../../parser/Parser.h"

using namespace cd12;
using namespace symboltable;
using namespace parser;

namespace fs = boost::filesystem;

struct SymbolTableFixture {
	SymbolTableFixture() {
	}

	SymbolTable stringAndNumberTable;
	SymbolTable defaultTable;
	SymbolTableManager tableManager;
};


BOOST_FIXTURE_TEST_SUITE(Parser_Test_Suite, SymbolTableFixture)

BOOST_AUTO_TEST_CASE(gdecls) {
	std::stringstream program;
	program << "PROGRAM id\n"
			<< "constant x = 5, y = 10\n";
			//<< "global var1";
	scanner::Scanner scanner(program.str(), tableManager);
	Parser parser(scanner, tableManager);

	Node* progNode = parser.parse();
	Node* gdecls = progNode->child(0);
	Node* cdecs = gdecls->child(0);
		Node* const1 = cdecs->child(0);
		Node* cdecs2 = cdecs->child(1);
			Node* const2 = cdecs->child(0);
	//Node* nvdecs = gdecls->child(1);
	BOOST_REQUIRE(progNode->id() == NPROG);
	BOOST_REQUIRE(gdecls->id() == NGDECS);
	BOOST_REQUIRE(cdecs->id() == NCDECS);
	BOOST_REQUIRE(const1->id() == NCONST);
	BOOST_REQUIRE(cdecs2->id() == NCDECS);
	BOOST_REQUIRE(const2->id() == NCONST);
}

BOOST_AUTO_TEST_SUITE_END()
