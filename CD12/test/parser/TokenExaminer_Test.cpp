#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#   define BOOST_TEST_MODULE Main
#endif
#include <algorithm>
#include <fstream>
#include <sstream>
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "../../scanner/Scanner.h"
#include "../../scanner/utility/Utility.h"
#include "../../symboltable/SymbolTableManager.h"
#include "../../parser/TokenExaminer.h"

using namespace cd12;
using namespace symboltable;
using namespace parser;

namespace fs = boost::filesystem;

struct SymbolTableFixture {
	SymbolTableFixture() {
	}

	SymbolTable stringAndNumberTable;
	SymbolTable defaultTable;
	SymbolTableManager tableManager;
};

BOOST_FIXTURE_TEST_SUITE(TokenExaminer_Test_Suite, SymbolTableFixture)

BOOST_AUTO_TEST_CASE(basic_input) {
	scanner::Scanner scanner("something 1.23 PROGRAM", tableManager);
	TokenExaminer examiner(scanner);

	BOOST_CHECK_EQUAL(examiner.next()->id(), scanner::TID);
	BOOST_CHECK_EQUAL(examiner.get()->id(), scanner::TID);
	BOOST_CHECK_EQUAL(examiner.peek()->id(), scanner::TFLIT);

	BOOST_CHECK_EQUAL(examiner.next()->id(), scanner::TFLIT);
	BOOST_CHECK_EQUAL(examiner.get()->id(), scanner::TFLIT);
	BOOST_CHECK_EQUAL(examiner.peek()->id(), scanner::TPROG);

	BOOST_CHECK_EQUAL(examiner.next()->id(), scanner::TPROG);
	BOOST_CHECK_EQUAL(examiner.get()->id(), scanner::TPROG);
}

BOOST_AUTO_TEST_CASE(looped_data) {
	scanner::Scanner scanner("something 1.23 PROGRAM", tableManager);
	TokenExaminer examiner(scanner);

	int count = 0;
	while(examiner.hasNext()) {
		count += 1;
		examiner.next();
		examiner.peek();
		examiner.peek();
		examiner.get();
		examiner.get();
	}
	BOOST_CHECK_EQUAL(count, 3);
}
BOOST_AUTO_TEST_SUITE_END()
