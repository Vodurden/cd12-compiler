#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#   define BOOST_TEST_MODULE Main
#endif
#include <algorithm>
#include <fstream>
#include <sstream>
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "../../scanner/Scanner.h"
#include "../../scanner/utility/Utility.h"
#include "../../symboltable/SymbolTableManager.h"

using namespace cd12;
using namespace symboltable;
using namespace scanner;

namespace fs = boost::filesystem;

struct SymbolTableFixture {
	SymbolTableFixture() {
	}

	SymbolTable stringAndNumberTable;
	SymbolTable defaultTable;
	SymbolTableManager tableManager;
};

BOOST_FIXTURE_TEST_SUITE(Scanner_Test_Suite, SymbolTableFixture)

BOOST_AUTO_TEST_CASE(emptyParse) {
	Scanner scanner("", tableManager);

	BOOST_CHECK_EQUAL(scanner.parseNext()->id(), TEOF);
}

BOOST_AUTO_TEST_CASE(ids) {
	Scanner scanner("these are some ids", tableManager);

	BOOST_CHECK_EQUAL(scanner.parseNext()->id(), TID);
	BOOST_CHECK_EQUAL(scanner.parseNext()->id(), TID);
	BOOST_CHECK_EQUAL(scanner.parseNext()->id(), TID);
	BOOST_CHECK_EQUAL(scanner.parseNext()->id(), TID);
	BOOST_CHECK_EQUAL(scanner.parseNext()->id(), TEOF);
}

BOOST_AUTO_TEST_CASE(keywords) {
	std::string keywords("program main end constant global procedure function local call if else with loop");
	keywords += " exit when export read display newline and or not length";

	TokenID ids[] = { TPROG, TMAIN, TEND, TCONS, TGLOB, TPROC, TFUNC, TLOCL, TCALL, TIF, TELSE, TWITH,
			TLOOP, TEXIT, TWHEN, TEXPT, TREAD, TDISP, TNEWL, TAND, TOR, TNOT, TLENG
	};

	Scanner scanner(keywords, tableManager);

	int currentId = 0;
	while(scanner.canParse()) {
		Token* tok = scanner.parseNext();
		TokenID currentID = ids[currentId];
		TokenID scannerID = tok->id();
		BOOST_CHECK_EQUAL(currentID, scannerID);

		++currentId;
	}
}

BOOST_AUTO_TEST_CASE(symbols) {
	std::string program("= [ ] { } ( ) ; , + - * / ^ < > . <= >= !~ ~=");

	TokenID ids[] = { TASGN, TLBRK, TRBRK, TLBRC, TRBRC, TLPAR, TRPAR, TSEMI, TCOMA, TPLUS, TMIN, TSTAR, TDIV, TUPAR,
			TLT, TGT, TDOT, TLE, TGE, TNEE, TEEQ
	};

	Scanner scanner(program, tableManager);

	int currentId = 0;
	while(scanner.canParse()) {
		Token* tok = scanner.parseNext();
		BOOST_CHECK_EQUAL(tok->id(), ids[currentId]);

		++currentId;
	}
}

BOOST_AUTO_TEST_CASE(numbers) {
	std::string program(".5 .50 100 200.0 300.5");

	Scanner scanner(program, tableManager);

	while(scanner.canParse()) {
		Token* tok = scanner.parseNext();
		BOOST_CHECK_EQUAL(tok->id(), TFLIT);
	}
}

BOOST_AUTO_TEST_CASE(strings) {
	std::string program("\"I am a test string\" \"So am I\"");

	Scanner scanner(program, tableManager);

	while(scanner.canParse()) {
		BOOST_CHECK_EQUAL(scanner.parseNext()->id(), TSTR);
	}
}

BOOST_AUTO_TEST_CASE(comments) {
	std::string program("++ I am a comment");

	Scanner scanner(program, tableManager);

	while(scanner.canParse()) {
		BOOST_CHECK_EQUAL(scanner.parseNext()->id(), TEOF);
	}
}

BOOST_AUTO_TEST_CASE(errors) {
	std::string program("@#~$%_?`!&|\\:''");

	Scanner scanner(program, tableManager);

	while(scanner.canParse()) {
		TokenID id = scanner.parseNext()->id();
		BOOST_REQUIRE(id == TUNDF || id == TEOF);
	}
}

BOOST_AUTO_TEST_CASE(expected_file_outputs) {
	std::string work_directory = "test/scanner/data";

	// Check that all the working lexical examples actually work
	fs::directory_iterator startDirItr(work_directory);
	fs::directory_iterator endDirItr;
	std::vector<fs::path> tests(startDirItr, endDirItr);

	for(auto test = tests.begin(); test != tests.end(); ++test) {
		fs::path basePath = *test;
		fs::ifstream inputPath((basePath / fs::path("input")));
		fs::ifstream expectedPath(basePath / fs::path("expected"));

		// Construct the expected mappings
		std::istreambuf_iterator<char> startInput(inputPath);
		std::istreambuf_iterator<char> endInput;

		std::istream_iterator<std::string> startExpected(expectedPath);
		std::istream_iterator<std::string> endExpected;

		std::vector<std::string> expected(startExpected, endExpected);
		std::string program(startInput, endInput);

		Scanner scanner(program, tableManager);

		BOOST_MESSAGE(std::string("Checking program: ") + test->string());
		for(auto expectedToken = expected.begin(); expectedToken != expected.end(); ++expectedToken) {
			Token* tok = scanner.parseNext();
			TokenID expected = Utility::getTokenID(*expectedToken);

			BOOST_CHECK_EQUAL(tok->id(), expected);
		}
	}
}

BOOST_AUTO_TEST_SUITE_END()
