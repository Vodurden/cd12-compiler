#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#   define BOOST_TEST_MODULE Main
#endif
#include <algorithm>
#include <fstream>
#include <sstream>
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "../../scanner/Scanner.h"
#include "../../scanner/filter/UndefinedFilter.h"
#include "../../scanner/utility/Utility.h"
#include "../../symboltable/SymbolTableManager.h"

using namespace cd12;
using namespace symboltable;
using namespace scanner;

namespace fs = boost::filesystem;

struct SymbolTableFixture {
	SymbolTableFixture() {
	}

	SymbolTable stringAndNumberTable;
	SymbolTable defaultTable;
	SymbolTableManager tableManager;
};

BOOST_FIXTURE_TEST_SUITE(UndefinedFilter_Test_Suite, SymbolTableFixture)

BOOST_AUTO_TEST_CASE(filters_undefined) {
	Scanner scanner("! something !", tableManager);
	filter::UndefinedFilter filter(scanner);

	while(filter.canParse()) {
		Token* tok = filter.parseNext();
		BOOST_REQUIRE(tok->id() != TUNDF);
	}
}

BOOST_AUTO_TEST_SUITE_END()
