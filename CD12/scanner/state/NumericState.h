#ifndef CD12_SCANNER_STATE_NUMERICSTATE_H
#define CD12_SCANNER_STATE_NUMERICSTATE_H
#include "State.h"
#include "States.h"
namespace cd12 {
	namespace scanner {
		class NumericState : public State {
			public:
				State* parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols);		};
	}
}
#endif
