#include "StringState.h"

using namespace cd12;
using namespace scanner;

State* StringState::parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols) {
	// Add the starting quote to the buffer
	buffer += examiner.get();
	examiner.next();

	char data = examiner.get();
	examiner.next();

	while(!Utility::isStringTerminator(data) && examiner.hasNext()) {
		if(data == '\n') {
			addError(buffer, examiner);
			return new DefaultState;
		}
		buffer += data;

		data = examiner.get();
		examiner.next();
	}


	if(Utility::isStringTerminator(data)) { // Make sure we only create a record if we hit a terminator
		buffer += data; // Add the ending terminator to the buffer
		addOutput(TSTR, buffer, examiner);
	} else {
		// If we're here it means we hit the end of the file
		// with
		addError(buffer, examiner);
	}
	return new DefaultState;
}
