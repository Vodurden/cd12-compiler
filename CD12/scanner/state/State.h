#ifndef CD12_SCANNER_STATE_H
#define CD12_SCANNER_STATE_H
#include <queue>
#include <iostream>
#include "../InputExaminer.h"
#include "../Token.h"
#include "../../symboltable/Record.h"
#include "../../symboltable/SymbolTableManager.h"
namespace cd12 {
	namespace scanner {
		class State {
			public:
				virtual ~State();
				virtual State* parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbolManager) = 0;

				bool hasOutput();
				Token* output();
			protected:

				void addOutput(TokenID id, const std::string& data, InputExaminer& examiner);
				void addOutput(TokenID id, InputExaminer& examiner);
				void addError(const std::string& error, InputExaminer& examiner);
			private:
				std::queue<Token*> outputs;
		};
	}
}
#endif
