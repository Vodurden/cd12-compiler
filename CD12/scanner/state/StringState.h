#ifndef CD12_SCANNER_STRINGSTATE_H
#define CD12_SCANNER_STRINGSTATE_H
#include <string>
#include "State.h"
#include "States.h"
#include "../TokenID.h"
#include "../Token.h"
namespace cd12 {
	namespace scanner {
		class StringState: public State {
			public:
				virtual State* parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols);
			private:
				std::string buffer;
		};
	}
}
#endif
