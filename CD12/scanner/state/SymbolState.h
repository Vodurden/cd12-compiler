/*
 * SymbolState.h
 *
 *  Created on: Aug 8, 2012
 *      Author: vodurden
 */

#ifndef CD12_SCANNER_STATE_SYMBOLSTATE_HD
#define CD12_SCANNER_STATE_SYMBOLSTATE_HD

#include "State.h"
#include "States.h"

namespace cd12 {
	namespace scanner {

		class SymbolState: public cd12::scanner::State {
			public:
				State* parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols);
		};

	}
}
#endif 
