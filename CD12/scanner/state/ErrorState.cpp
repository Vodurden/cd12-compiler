/*
 * ErrorState.cpp
 *
 *  Created on: Aug 10, 2012
 *      Author: vodurden
 */

#include <string>
#include "ErrorState.h"

using namespace cd12;
using namespace scanner;

State* cd12::scanner::ErrorState::parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols) {

	std::string buffer;

	// Consume input untill we hit a valid character
	do {
		buffer += examiner.get();
		examiner.next();
	} while(!Utility::isValidSourceCharacter(examiner.get()) && examiner.hasNext());

	addError(buffer, examiner); 
	return new DefaultState;
}
