/*
 * SymbolState.cpp
 *
 *  Created on: Aug 8, 2012
 *      Author: vodurden
 */

#include "SymbolState.h"

using namespace cd12;
using namespace scanner;

State* cd12::scanner::SymbolState::parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols) {
	// We know all symbols are either 1 or 2 character.
	// so lets look ahead and see what we're going to get

	// Check for two character symbols
	if (examiner.canPeek()) { // If we can't peek it must be one symbol
		std::string twochar = std::string(1, examiner.get()) + examiner.peek();

		// Check if this is the comment symbol
		if(Utility::isCommentStarter(twochar)) {
			return new CommentState;
		}

		// Check if this is a .x formatted number
		if(twochar[0] == '.' && Utility::isNumber(twochar[1])) {
			return new NumericState;
		}

		if (Utility::isSymbol(twochar)) {
			addOutput(Utility::symbolToToken(twochar), examiner);
			// Consume both chars
			examiner.next();
			examiner.next();
			return new DefaultState;
		}
	}

	// Since we're here it's probably a 1 character symbol
	if (Utility::isSymbol(examiner.get())) {
		addOutput(Utility::symbolToToken(examiner.get()), examiner);
		examiner.next(); // Consume both chars
		return new DefaultState;
	}

	// Now we really have no idea how we got here so return an error
	return new ErrorState;
}
