#include "DefaultState.h"

using namespace cd12;
using namespace scanner;

DefaultState::~DefaultState() {

}

State* DefaultState::parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols) {
	// If the examiner has ended just return this
	// prevents incorrect error reporting
	if (!examiner.hasNext()) {
		return this;
	}

	// Identifiers and Keywords
	if (Utility::isIdentifierStarter(examiner.get())) {
		return new IdentifierState;
	}

	// Symbols
	if (Utility::isSymbolStarter(examiner.get())) {
		return new SymbolState;
	}

	// Numbers
	if (Utility::isNumericStarter(examiner.get())) {
		return new NumericState;
	}

	// Strings
	if (Utility::isStringStarter(examiner.get())) {
		return new StringState;
	}

	// Ignore Whitespace
	if (isspace(examiner.get()) || examiner.get() == '\n') {
		examiner.next();
		return this;
	}

	// Anything else at this point is an error //return new ErrorState;
	return new ErrorState;
}
