#include "IdentifierState.h"

using namespace cd12;
using namespace scanner;

IdentifierState::IdentifierState() {
}

IdentifierState::~IdentifierState() {

}

State* IdentifierState::parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols) {
	while(Utility::isIdentifierCharacter(examiner.get())) {
		buffer += examiner.get();
		examiner.next();
	}

	// End of token. Time to output
	if (Utility::isKeyword(buffer)) {
		addOutput(Utility::keywordToToken(buffer), examiner);
	} else {
		addOutput(TID, buffer, examiner);
	}

	return new DefaultState;
}
