#ifndef CD12_SCANNER_IDENTIFIERSTATE_H
#define CD12_SCANNER_IDENTIFIERSTATE_H
#include <string>
#include "State.h"
#include "States.h"
namespace cd12 {
	namespace scanner {
		class IdentifierState: public State {
			public:
				IdentifierState();
				virtual ~IdentifierState();

				virtual State* parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols);
			private:
				std::string buffer;
		};
	}
}
#endif
