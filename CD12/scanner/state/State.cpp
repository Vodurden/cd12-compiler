#include "State.h"

using namespace cd12;
using namespace scanner;

State::~State() {
} // Required to create Virtual Table

bool State::hasOutput() {
	return !outputs.empty();
}

Token* State::output() {
	Token* output = outputs.front();
	outputs.pop();
	return output;
}

void cd12::scanner::State::addOutput(TokenID id, const std::string& data, InputExaminer& examiner) {
	symboltable::Record* record = new symboltable::Record(data);
	// TODO: Add to symbol table here
	Token* token = new Token(id, record, examiner.row(), examiner.col());
	outputs.push(token);
}

void cd12::scanner::State::addOutput(TokenID id, InputExaminer& examiner) {
	Token* token = new Token(id, examiner.row(), examiner.col());
	outputs.push(token);
}

void State::addError(const std::string& error, InputExaminer& examiner) {
	addOutput(TUNDF, error, examiner);
}
