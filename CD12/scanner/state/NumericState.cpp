#include "NumericState.h"

using namespace cd12;
using namespace scanner;

State* cd12::scanner::NumericState::parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols) {
	// Consume all numbers and a maximum of 1 dot
	std::string buffer("");
	bool seenDot = false;
	while(Utility::isNumber(examiner.get()) || examiner.get() == '.') {
		if(examiner.get() == '.') {
			if(seenDot) {
				break; // If we've seen the dot twice it's time to exit the loop
			}

			seenDot = true;
		}

		buffer += examiner.get();
		examiner.next();
	}

	// Format the number as a double
	double bufferAsDouble = Utility::lexical_cast<double>(buffer);
	std::string formattedBuffer = Utility::toString(bufferAsDouble);

	addOutput(TFLIT, formattedBuffer, examiner);
	return new DefaultState;
}
