#ifndef CD12_SCANNER_STATE_ERRORSTATE_H 
#define CD12_SCANNER_STATE_ERRORSTATE_H
#include "State.h"
#include "States.h"

namespace cd12 {
	namespace scanner {

		class ErrorState: public cd12::scanner::State {
			public:
				State* parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols);
		};

	} 
} 
#endif 
