#ifndef CD12_SCANNER_DEFAULTSTATE_H
#define CD12_SCANNER_DEFAULTSTATE_H
#include <cctype>
#include <iostream>
#include "State.h"
#include "States.h"
#include "../utility/utility.h"
namespace cd12 {
	namespace scanner {
		class DefaultState: public State {
			public:
				~DefaultState();

				State* parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols);
		};
	}
}
#endif
