#include "CommentState.h"

using namespace cd12;
using namespace scanner;

State* cd12::scanner::CommentState::parse(InputExaminer& examiner, symboltable::SymbolTableManager& symbols) {
	// Since we're a comment we just consume until we hit newline
	while(examiner.get() != '\n' && examiner.hasNext()) {
		examiner.next();
	}

	return new DefaultState;
}
