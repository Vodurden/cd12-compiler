#ifndef CD12_SCANNER_FILTER_UNDEFINEDFILTER_HC
#define CD12_SCANNER_FILTER_UNDEFINEDFILTER_HC
#include <iostream>
#include "../IScanner.h"

namespace cd12 {
namespace scanner {
namespace filter {

	class UndefinedFilter : public IScanner {
		public:
			UndefinedFilter(IScanner& scanner);
			virtual ~UndefinedFilter();

			bool canParse();
			Token* parseNext();
		private:
			IScanner& m_scanner;
	};

}
}
}
#endif
