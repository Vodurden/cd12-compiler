/*
 * UndefinedFilter.cpp
 *
 *  Created on: Sep 9, 2012
 *      Author: vodurden
 */

#include "UndefinedFilter.h"

namespace cd12 {
namespace scanner {
namespace filter {

UndefinedFilter::UndefinedFilter(IScanner& scanner) :
	m_scanner(scanner) {

}

UndefinedFilter::~UndefinedFilter() {
}

bool UndefinedFilter::canParse() {
	return m_scanner.canParse();
}

Token* UndefinedFilter::parseNext() {
	Token* tok = 0;
	do {
		tok = m_scanner.parseNext();
		if(tok->id() == TUNDF) {
			std::cout << "lexical error: " << tok->record()->getID();
			std::cout << " on [line:col] " << tok->row() << ":" << tok->col() << std::endl;
		}
	} while(tok->id() == TUNDF);

	return tok;
}

}
}
} /* namespace cd12 */
