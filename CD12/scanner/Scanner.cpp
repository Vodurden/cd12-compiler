#include "Scanner.h"

using namespace cd12;
using namespace scanner;

cd12::scanner::Scanner::Scanner(std::string input,
		symboltable::SymbolTableManager& symbolTableManager) :
		m_examiner(input), m_symbolTableManager(symbolTableManager), m_doneEOF(false) {
}

cd12::scanner::Scanner::~Scanner() {
}

Token* Scanner::parseNext() {
	State* currentState = new DefaultState;
	while (m_examiner.hasNext()) {
		if (m_examiner.get() == '\r') {
			m_examiner.next();
			continue; // Ignore return carriage, \n is enough.
		}

		State* nextState = currentState->parse(m_examiner, m_symbolTableManager);

		// If we've found a token return it and exit
		Token* returnValue = 0;
		if (currentState->hasOutput()) {
			// If the token has data add it's record to the symbolTable
			returnValue = currentState->output();
		}

		// If we need to switch states, delete the old one
		if (currentState != nextState) {
			delete currentState;
			currentState = nextState;
		}

		if (returnValue) {
			return returnValue;
		}
	}

	// Send an empty char to the final state to
	// flush any variables it might have
	if (!m_doneEOF) {
		currentState->parse(m_examiner, m_symbolTableManager);
		m_doneEOF = true;
		if (currentState->hasOutput()) {
			return currentState->output();
		}
	}

	// Since we've hit the End of file, return the EOF token
	return new Token(TEOF, m_examiner.row(), m_examiner.col());
}

bool Scanner::canParse() {
	return m_examiner.hasNext();
}
