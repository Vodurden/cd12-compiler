#include "Token.h"

cd12::scanner::Token::Token(TokenID id, symboltable::Record* record, int row, int col) :
		m_id(id), m_symbolTableRecord(record), m_row(row), m_col(col) {
}

cd12::scanner::Token::Token(TokenID id, int row, int col) :
		m_id(id), m_symbolTableRecord(0), m_row(row), m_col(col) {
}

cd12::symboltable::Record* cd12::scanner::Token::record() {
	return m_symbolTableRecord;
}

int cd12::scanner::Token::col() {
	return m_col;
}

int cd12::scanner::Token::row() {
	return m_row;
}

bool cd12::scanner::Token::hasRecord() {
	return (m_symbolTableRecord != 0);
}

cd12::scanner::TokenID cd12::scanner::Token::id() {
	return m_id;
}

std::string cd12::scanner::Token::toString() {
	std::stringstream output;
	std::string id = Utility::getTokenName(m_id);

	output << std::left;

	// If the id is exactly 6 chars it needs to be
	// padded to 7
	if(id.length() == 6) {
		output << std::setw(7);
	} else {
		output << std::setw(6);
	}

	output << id;

	if(hasRecord()) {
		std::string data = m_symbolTableRecord->getID();
		int padding = Utility::roundToNearestMultiple(data.length(), 6);

		if(data.length() % 6 == 0) {
			padding += 1;
		}

		output << std::setw(padding);
		output << data;
	}

	return output.str();
}
