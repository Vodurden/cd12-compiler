#ifndef CD12_SCANNER_ISCANNER_H
#define CD12_SCANNER_ISCANNER_H
#include "Token.h"
namespace cd12 {
	namespace scanner {
		// Scanner interface class
		class IScanner {
			public:
				virtual ~IScanner();

				virtual Token* parseNext() = 0;
				virtual bool canParse() = 0;
		};
	}
}
#endif
