#ifndef CD12_SCANNER_INPUTEXAMINER_H
#define CD12_SCANNER_INPUTEXAMINER_H
#include <string>
namespace cd12 {
	namespace scanner {
		class InputExaminer {
			public:
				InputExaminer(const std::string& input);

				bool done();

				bool hasNext();
				char next(); // Move to the next char and return it
				char get(); // Get the current character

				bool canPeek(int offset = 1);
				char peek(int offset = 1);

				int row();
				int col();

			private:
				std::string m_input;
				std::string::iterator m_currentPos;

				int m_row, m_col;
		};
	}
}
#endif
