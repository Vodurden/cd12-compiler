#ifndef CD12_SCANNER_SCANNER_H
#define CD12_SCANNER_SCANNER_H
#include <string>
#include <map>
#include <vector>
#include <functional>
#include "state/State.h"
#include "state/DefaultState.h"
#include "../symboltable/SymbolTable.h"
#include "IScanner.h"
#include "InputExaminer.h"
#include "Token.h"
namespace cd12 {
	namespace scanner {
		class Scanner : public IScanner {
			public:
				Scanner(std::string input,
						symboltable::SymbolTableManager& symbolTableManager);
				~Scanner();

				Token* parseNext();
				bool canParse();
			private:
				InputExaminer m_examiner;
				symboltable::SymbolTableManager& m_symbolTableManager;

				bool m_doneEOF;
		};
	}
}
#endif
