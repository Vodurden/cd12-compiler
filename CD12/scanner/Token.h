#ifndef CD12_SCANNER_TOKEN_H
#define CD12_SCANNER_TOKEN_H
#include <cmath>
#include <string>
#include "../symboltable/Record.h"
#include "utility/Utility.h"
#include "TokenID.h"
namespace cd12 {
	namespace scanner {
		class Token {
			public:

				Token(TokenID id, symboltable::Record* record, int row, int col);
				Token(TokenID id, int row, int col);

				int col();
				int row();

				TokenID id();

				bool hasRecord();
				symboltable::Record* record();

				std::string toString();
			private:
				TokenID m_id;

				symboltable::Record* m_symbolTableRecord;

				int m_row, m_col;
		};
	}
}
#endif
