#ifndef CD12_SCANNER_UTILITY_H
#define CD12_SCANNER_UTILITY_H
#include <string>
#include <sstream>
#include <iomanip>
#include <map>
#include <cctype>
#include <algorithm>
#include "../Token.h"
#include "../TokenID.h"
namespace cd12 {
	namespace scanner {
		class Utility {
			public:
				// Returns true if a character can
				// start a identifier token
				static bool isIdentifierStarter(char c);

				// Returns true if a character is
				// a valid identifier character
				static bool isIdentifierCharacter(char c);

				// Returns true if a character
				// is a valid keyword
				static bool isKeyword(const std::string& data);

				// Converts a string to it's equivalent token ID
				static TokenID keywordToToken(const std::string& data);

				// Returns true if a character can
				// start a symbol token
				static bool isSymbolStarter(char c);

				static bool isSymbol(const std::string& data);
				static bool isSymbol(char data);

				// Converts a symbol string to it's equivalent ID
				static TokenID symbolToToken(const std::string& data);
				static TokenID symbolToToken(char data);

				// Returns true if a character can
				// start a number token
				static bool isNumericStarter(char c);

				// Returns true if the character is a number
				static bool isNumber(char c);

				// Returns true if a character can
				// start a string
				static bool isStringStarter(char c);

				// Returns true if a character can
				// end a string
				static bool isStringTerminator(char c);

				static bool isCommentStarter(const std::string& data);

				// Returns true if the character is any
				// valid character in the source code
				// (letters, numbers, valid symbols, white space)
				static bool isValidSourceCharacter(char c);

				static bool isEOF(char c);

				static std::string toLower(const std::string& input);

				static std::string getTokenName(TokenID id);

				static TokenID getTokenID(const std::string& str);

				static int roundToNearestMultiple(int number, int multiple);

				template <typename To, typename From>
				static To lexical_cast(const From &input) {
					std::stringstream stream;
					To returnValue;

					stream << input;
					stream >> returnValue;

					return returnValue;
				}

				// special formatting override
				static std::string toString(double input) {
					std::stringstream stream;

					// Check if the double is in the format x.0 (is an integer)
					if(std::ceil(input) == input) {
						// Force the .0 on an integer
						stream << std::setiosflags(std::ios::fixed | std::ios::showpoint);
						stream << std::setprecision(1);
					} else {
						stream.precision(15);
					} 

					stream << input;
					return stream.str();
				}

				static std::map<std::string, TokenID>& getKeywords();
				static std::map<std::string, TokenID>& getSymbols();
			private:

				static std::map<std::string, TokenID> s_keywords;
				static std::map<std::string, TokenID> s_symbols;

				static const char* TOKEN_ID_NAMES[];
		};
	}
}
#endif
