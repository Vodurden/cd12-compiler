#include "Utility.h"

using namespace cd12;
using namespace scanner;

std::map<std::string, TokenID> Utility::s_keywords;
std::map<std::string, TokenID> Utility::s_symbols;

const char* Utility::TOKEN_ID_NAMES[] = {
		"TEOF", // Token value for end of file
		// The 23 keywords
		"TPROG", "TCONS", "TGLOB", "TPROC", "TFUNC", "TMAIN", "TEND", "TWITH",
		"TLOOP", "TEXIT", "TWHEN", "TIF", "TELSE", "TEXPT", "TCALL", "TREAD",
		"TDISP", "TNEWL", "TLOCL", "TAND", "TOR", "TNOT", "TLENG",
		// the operators and delimiters : 21
		"TASGN", "TLBRC", "TRBRC", "TLBRK", "TRBRK", "TLPAR", "TRPAR", "TSEMI",
		"TCOMA", "TPLUS", "TMIN", "TSTAR", "TDIV", "TUPAR", "TLT", "TGT",
		"TDOT", "TLE", "TGE", "TNEE", "TEEQ",
		// the tokens which need tuple values : 4
		"TID", "TFLIT", "TSTR", "TUNDF" };

const int TOKEN_ID_NAMES_LENGTH = TOKENID_ENUM_END + 1;

std::map<std::string, TokenID>& cd12::scanner::Utility::getKeywords() {
	// Check if the keywords are filled, if not fill them
	if (s_keywords.empty()) {
		s_keywords["program"] = TPROG;
		s_keywords["constant"] = TCONS;
		s_keywords["global"] = TGLOB;
		s_keywords["procedure"] = TPROC;
		s_keywords["function"] = TFUNC;
		s_keywords["local"] = TLOCL;
		s_keywords["main"] = TMAIN;
		s_keywords["end"] = TEND;
		s_keywords["with"] = TWITH;
		s_keywords["loop"] = TLOOP;
		s_keywords["if"] = TIF;
		s_keywords["else"] = TELSE;
		s_keywords["exit"] = TEXIT;
		s_keywords["when"] = TWHEN;
		s_keywords["call"] = TCALL;
		s_keywords["export"] = TEXPT;
		s_keywords["read"] = TREAD;
		s_keywords["display"] = TDISP;
		s_keywords["newline"] = TNEWL;
		s_keywords["and"] = TAND;
		s_keywords["or"] = TOR;
		s_keywords["not"] = TNOT;
		s_keywords["length"] = TLENG;
	}

	return s_keywords;
}

// TODO: Optimize, this is horrible.
std::string cd12::scanner::Utility::toLower(const std::string& input) {
	std::string output("");
	for (std::string::const_iterator it = input.begin(); it != input.end();
			++it) {
		char d = *it;
		output += tolower(d);
	}

	return output;
}

std::string cd12::scanner::Utility::getTokenName(TokenID id) {
	return TOKEN_ID_NAMES[id];
}

TokenID cd12::scanner::Utility::getTokenID(const std::string& str) {
	for(int i = 0; i < TOKEN_ID_NAMES_LENGTH; ++i) {
		if(str == TOKEN_ID_NAMES[i] ) {
			return static_cast<TokenID>(i);
		}
	}

	return static_cast<TokenID>(0);
}

bool cd12::scanner::Utility::isSymbol(const std::string& data) {
	std::map<std::string, TokenID>& symbols = getSymbols();
	return symbols.find(data) != symbols.end();
}

bool cd12::scanner::Utility::isSymbol(char data) {
	return isSymbol(std::string(1, data));
}

bool cd12::scanner::Utility::isNumber(char c) {
	return isdigit(c);
}

bool cd12::scanner::Utility::isCommentStarter(const std::string& data) {
	return (data == "++");
}

bool cd12::scanner::Utility::isValidSourceCharacter(char c) {
	return (isalnum(c) | isspace(c) | isStringStarter(c) | isSymbolStarter(c) | isEOF(c));
}

int cd12::scanner::Utility::roundToNearestMultiple(int number, int multiple) {
	if(multiple == 0) return number;

	int remainder = number % multiple;
	if(remainder == 0) {
		return number;
	}

	return number + multiple - remainder;
}


std::map<std::string, TokenID>& cd12::scanner::Utility::getSymbols() {
	// Check if the symbols are filled, if not fill them
	if (s_symbols.empty()) {
		s_symbols["="] = TASGN;
		s_symbols["{"] = TLBRC;
		s_symbols["}"] = TRBRC;
		s_symbols["["] = TLBRK;
		s_symbols["]"] = TRBRK;
		s_symbols["("] = TLPAR;
		s_symbols[")"] = TRPAR;
		s_symbols[";"] = TSEMI;
		s_symbols[","] = TCOMA;
		s_symbols["+"] = TPLUS;
		s_symbols["-"] = TMIN;
		s_symbols["*"] = TSTAR;
		s_symbols["/"] = TDIV;
		s_symbols["^"] = TUPAR;
		s_symbols["<"] = TLT;
		s_symbols[">"] = TGT;
		s_symbols["."] = TDOT;
		s_symbols["<="] = TLE;
		s_symbols[">="] = TGE;
		s_symbols["!~"] = TNEE;
		s_symbols["~="] = TEEQ;
	}

	return s_symbols;
}

bool cd12::scanner::Utility::isIdentifierStarter(char c) {
	return std::isalpha(c);
}

bool cd12::scanner::Utility::isIdentifierCharacter(char c) {
	return std::isalnum(c);
}

bool cd12::scanner::Utility::isKeyword(const std::string& data) {
	std::map<std::string, TokenID>& keywords = getKeywords();
	return keywords.find(toLower(data)) != keywords.end();
}

bool cd12::scanner::Utility::isSymbolStarter(char c) {
	// This could be replaced with another memory map.
	// but for now we'll just check the symbol map
	std::map<std::string, TokenID> symbols = getSymbols();
	for (std::map<std::string, TokenID>::iterator it = symbols.begin();
			it != symbols.end(); ++it) {

		char start = it->first[0];
		if (c == start)
			return true;
	}

	return false;
}

bool cd12::scanner::Utility::isEOF(char c)  {
	return c == static_cast<char>(0);
}

bool cd12::scanner::Utility::isNumericStarter(char c) {
	return std::isdigit(c);
}

bool cd12::scanner::Utility::isStringStarter(char c) {
	return c == '"';
}

TokenID cd12::scanner::Utility::keywordToToken(const std::string& data) {
	return getKeywords()[toLower(data)];
}

TokenID cd12::scanner::Utility::symbolToToken(const std::string& data) {
	return getSymbols()[data];
}

TokenID cd12::scanner::Utility::symbolToToken(char data) {
	return symbolToToken(std::string(1, data));
}

bool cd12::scanner::Utility::isStringTerminator(char c) {
	return c == '"';
}

