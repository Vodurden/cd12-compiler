#ifndef CD12_SCANNER_TOKENID_H
#define CD12_SCANNER_TOKENID_H
namespace cd12 {
	namespace scanner {
		enum TokenID {
			TOKENID_ENUM_START = 0,
			TEOF = 0,	  // Token value for end of file
			// The 23 keywords
			TPROG = 1,
			TCONS = 2,
			TGLOB = 3,
			TPROC = 4,
			TFUNC = 5,
			TMAIN = 6,
			TEND = 7,
			TWITH = 8,
			TLOOP = 9,
			TEXIT = 10,
			TWHEN = 11,
			TIF = 12,
			TELSE = 13,
			TEXPT = 14,
			TCALL = 15,
			TREAD = 16,
			TDISP = 17,
			TNEWL = 18,
			TLOCL = 19,
			TAND = 20,
			TOR = 21,
			TNOT = 22,
			TLENG = 23,
			// the operators and delimiters
			TASGN = 24,
			TLBRC = 25,
			TRBRC = 26,
			TLBRK = 27,
			TRBRK = 28,
			TLPAR = 29,
			TRPAR = 30,
			TSEMI = 31,
			TCOMA = 32,
			TPLUS = 33,
			TMIN = 34,
			TSTAR = 35,
			TDIV = 36,
			TUPAR = 37,
			TLT = 38,
			TGT = 39,
			TDOT = 40,
			TLE = 41,
			TGE = 42,
			TNEE = 43,
			TEEQ = 44,
			// the tokens which need tuple values
			TID = 45,
			TFLIT = 46,
			TSTR = 47,
			TUNDF = 48,
			TOKENID_ENUM_END = 48
		};
	}
}
#endif
