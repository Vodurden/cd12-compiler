#include "InputExaminer.h"

using namespace cd12;
using namespace scanner;

InputExaminer::InputExaminer(const std::string& input) :
		m_input(input), m_row(1), m_col(1) {

	m_currentPos = m_input.begin();
}

bool InputExaminer::done() {
	return (m_currentPos == m_input.end());
}

bool InputExaminer::hasNext() {
	return (m_currentPos != m_input.end());
}

char InputExaminer::next() {
	++m_currentPos;

	if (*m_currentPos == '\n') {
		m_row += 1;
		m_col = 1;
	} else {
		m_col += 1;
	}

	return get();
}

char InputExaminer::get() {
	if (m_currentPos == m_input.end()) {
		return static_cast<char>(0);
	}

	return *m_currentPos;
}

bool InputExaminer::canPeek(int offset) {
	return !((m_currentPos + offset) == m_input.end());
}

char InputExaminer::peek(int offset) {
	return *(m_currentPos + offset);
}

int cd12::scanner::InputExaminer::row() {
	return m_row;
}

int cd12::scanner::InputExaminer::col() {
	return m_col;
}
