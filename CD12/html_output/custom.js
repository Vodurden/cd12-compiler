$(function () {
	$('#chart').jstree({
		"core" : {html_titles: true, animation: 100},
		"plugins" : ["themes","html_data","ui","crrm"],
	});
	
	var tree = $('#chart');
    tree.bind("loaded.jstree", function (event, data) {
        tree.jstree("open_all");
    });
});