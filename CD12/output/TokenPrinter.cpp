/*
 * TokenPrinter.cpp
 *
 *  Created on: Aug 10, 2012
 *      Author: vodurden
 */

#include "TokenPrinter.h"

namespace cd12 {
	namespace output {

		TokenPrinter::TokenPrinter(scanner::Scanner& scanner) :
			m_scanner(scanner) {

		}

		TokenPrinter::~TokenPrinter() {
		}

		void TokenPrinter::print() {
			int currentCol = 0;
			cd12::scanner::Token* tok = m_scanner.parseNext();
			bool eofPrinted = false;

			while(!eofPrinted || tok->id() != cd12::scanner::TEOF){
				if(tok->id() == cd12::scanner::TEOF) {
					eofPrinted = true;
				}

				// Check for undefined tokens
				if(tok->id() == cd12::scanner::TUNDF) {
					if(currentCol > 0) {
						std::cout << std::endl;
					}

					std::cout << "lexical error: " << tok->record()->getID() << std::endl;

					currentCol = 0;
					tok = m_scanner.parseNext();
					continue;
				}

				std::string output(tok->toString());
				std::cout << output;

				currentCol += output.length();
				if(currentCol > 60) {
					currentCol = 0;
					std::cout << std::endl;
				}

				tok = m_scanner.parseNext();
			}
		}
	} 
}
