#ifndef CD12_OUTPUT_TOKENPRINTER_H 
#define CD12_OUTPUT_TOKENPRINTER_H
#include "../scanner/Scanner.h"

namespace cd12 {
	namespace output {

		// Prints the token stream to the standard output
		class TokenPrinter {
			public:
				TokenPrinter(scanner::Scanner& scanner);
				~TokenPrinter();

				void print();
			private:
				scanner::Scanner& m_scanner;
		};

	}
} 
#endif
