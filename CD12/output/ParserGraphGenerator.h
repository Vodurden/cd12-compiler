/*
 * ParserGraphGenerator.h
 *
 *  Created on: Sep 12, 2012
 *      Author: vodurden
 */

#ifndef CD12_OUTPUT_PARSERGRAPHGENERATOR_H 
#define CD12_OUTPUT_PARSERGRAPHGENERATOR_H
#include "HTMLGenerator.h"
#include "../parser/Parser.h"
#include "../parser/utility/Utility.h"

namespace cd12 {
namespace output {

	class ParserGraphGenerator : public HTMLGenerator {
		public:
			ParserGraphGenerator(const std::string& outputDir, parser::Node* root);
			virtual ~ParserGraphGenerator();

		protected:
			std::string generateBody();
		private:
			parser::Node* m_root;

			std::string parseNode(parser::Node* current);
	};

}
}
#endif
