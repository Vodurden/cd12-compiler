/*
 * ParserGraphGenerator.cpp
 *
 *  Created on: Sep 12, 2012
 *      Author: vodurden
 */

#include "ParserGraphGenerator.h"

namespace cd12 {
namespace output {

ParserGraphGenerator::ParserGraphGenerator(const std::string& outputDir, parser::Node* root) :
	HTMLGenerator(outputDir),
	m_root(root) {

}

ParserGraphGenerator::~ParserGraphGenerator() {
}

std::string ParserGraphGenerator::generateBody() {
	std::stringstream body;
	body
		<< "<ul id=\"chart\" class=\"root\"><a href=\"#\">CD12</a><ul>"
		<< parseNode(m_root)
		<< "</ul></ul>";

	return body.str();
}

std::string ParserGraphGenerator::parseNode(parser::Node* current) {
	std::stringstream node;
	node << "<li class=\"node\"><a href=\"#\">" << cd12::parser::Utility::idToString(current->id()) << "</a>" << std::endl;

	node << "<ul>" << std::endl;
	if(current->record() != 0) {
		node << "<li class=\"data\"><a href=\"#\">" << current->record()->getID() << "</a></li>" << std::endl;
	}

	if(current->type() != 0) {
		node << "<li class=\"data\"><a href=\"#\">" << current->type()->getID() << "</a></li>" << std::endl;
	}

	if(current->hasChildren()) {
		// Type and record

		for(auto child = current->begin(); child != current->end(); ++child) {
			node << parseNode(*child) << "\n";
		}
	}
	node << "</ul>" << std::endl;

	node << "</li>" << std::endl;

	return node.str();
}

}
} /* namespace cd12 */
