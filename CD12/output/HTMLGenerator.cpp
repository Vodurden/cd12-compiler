/*
 * HTMLGenerator.cpp
 *
 *  Created on: Sep 12, 2012
 *      Author: vodurden
 */

#include "HTMLGenerator.h"

namespace cd12 {
namespace output {

HTMLGenerator::HTMLGenerator(const std::string& outputDir) :
	m_outputDir(outputDir) {

}

HTMLGenerator::~HTMLGenerator() {
}

// Public API
void HTMLGenerator::generate(const std::string& filename) {
	std::stringstream document;
	document
	<< "<html>\n"
	<< getHeader()
	<< "<body>\n"
		<< generateBody()
	<< "</body>\n"
	<< "</html>\n";

	// Write the data to a file
	std::cout << "Writing ouput file to " << getOutputDir() + "/" + filename << std::endl;
	std::ofstream file(getOutputDir() + "/" + filename);
	file << document.str();
	file.flush();
	file.close();
}

// Protected API
const std::string& HTMLGenerator::getOutputDir() {
	return m_outputDir;
}

const std::string& HTMLGenerator::getHeader() {
	if(m_header.empty()) {
		m_header = generateHeader();
	}

	return m_header;
}

// Private
std::string HTMLGenerator::generateHeader() {
	std::stringstream header;
	header
	<< "<head>\n"
		<< "\t<script type=\"text/javascript\" src=\"jquery-1.8.1.min.js\"></script>\n"
		<< "\t<script type=\"text/javascript\" src=\"jquery.jstree.js\"></script>\n"
		<< "\t<script type=\"text/javascript\" src=\"custom.js\"></script>\n"
		<< "\t<link rel=\"stylesheet\" href=\"css/custom.css\"/>\n"
		<< "\t<link rel=\"stylesheet\" href=\"css/bootstrap.min.css\"/>\n"
	<< "</head>\n";

	return header.str();
}

}
}
