/*
 * HTMLGenerator.h
 *
 *  Created on: Sep 12, 2012
 *      Author: vodurden
 */

#ifndef CD12_OUTPUT_HTMLGENERATOR_H
#define CD12_OUTPUT_HTMLGENERATOR_H
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

namespace cd12 {
namespace output {

	class HTMLGenerator {
		public:
			HTMLGenerator(const std::string& outputDir);
			virtual ~HTMLGenerator();

			void generate(const std::string& filename);
		protected:
			virtual std::string generateBody() = 0;

			const std::string& getOutputDir();

			const std::string& getHeader();
		private:
			std::string m_outputDir;

			std::string generateHeader();
			std::string m_header;
	};

}
}
#endif
