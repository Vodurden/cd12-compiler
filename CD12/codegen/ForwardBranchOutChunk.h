#ifndef CD12_CODEGEN_FORWARDBRANCHOUTCHUNK_H
#define CD12_CODEGEN_FORWARDBRANCHOUTCHUNK_H
#include "OutChunk.h"
#include "Instruction.h"
namespace cd12 {
namespace codegen {
	class ForwardBranchOutChunk : public OutChunk {
	public:
		ForwardBranchOutChunk(int id);
		virtual ~ForwardBranchOutChunk();

		virtual void generate(std::ostream& out = std::cout);

		void resolve(int insCount);
		int getOffset();
	private:
		int m_id;
		int m_offset;
	};
}
}
#endif
