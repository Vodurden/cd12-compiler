#ifndef CD12_CODEGEN_INSTRUCTION_H
#define CD12_CODEGEN_INSTRUCTION_H
namespace cd12 {
namespace codegen {
namespace ins {
	enum Instruction {
		HALT = 0,
		NO = 1,
		TRAP = 2,
		ZERO = 3,

		A = 11,
		S = 12,
		M = 13,
		D = 14,

		POW = 16,
		CHS = 17,
		ABS = 18,

		GT = 21,
		GE = 22,
		LT = 23,
		LE = 24,
		EQ = 25,
		NE = 26,

		AND = 31,
		OR = 32,
		NOT = 33,
		BT = 35,
		BF = 36,
		BR = 37,

		L = 40,
		ST = 43,

		ALLOC = 52,
		ARRAY = 53,
		INDEX = 54,
		SIZE = 55,
		DUP = 56,

		READF = 61,
		VALPR = 62,
		STRPR = 63,
		CHRPR = 64,
		NEWLN = 65,
		SPACE = 67,

		RVAL = 70,
		RETN = 71,
		JS2 = 72,
	};

	enum ArgInstruction {
		LES = 41,
		LED = 42,
	};

	enum SymbolInstruction {
		LV = 8,
		LA = 9
	};
}
}
}
#endif
