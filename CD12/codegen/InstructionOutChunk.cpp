/*
 * InstructionOutChunk.cpp
 *
 *  Created on: Nov 4, 2012
 *      Author: vodurden
 */

#include "InstructionOutChunk.h"

using namespace cd12;
using namespace codegen;

InstructionOutChunk::InstructionOutChunk(ins::Instruction ins) :
		m_instruction(ins) {

}

InstructionOutChunk::~InstructionOutChunk() {
}

void InstructionOutChunk::generate(std::ostream& out) {
	out << m_instruction << " ";
}
