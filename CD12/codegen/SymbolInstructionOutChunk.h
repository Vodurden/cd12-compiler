#ifndef CD12_CODEGEN_SYMBOLINSTRUCTIONOUTCHUNK_H
#define CD12_CODEGEN_SYMBOLINSTRUCTIONOUTCHUNK_H
#include "OutChunk.h"
#include "Instruction.h"
#include "../symboltable/Record.h"
namespace cd12 {
namespace codegen {
	class SymbolInstructionOutChunk : public OutChunk {
	public:
		SymbolInstructionOutChunk (ins::SymbolInstruction in, symboltable::Record* record);
		virtual ~SymbolInstructionOutChunk();

		virtual void generate(std::ostream& out = std::cout);
	private:
		ins::SymbolInstruction m_instruction;
		symboltable::Record* m_record;
	};
}
}
#endif
