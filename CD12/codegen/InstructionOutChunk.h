#ifndef CD12_CODEGEN_INSTRUCTIONOUTCHUNK_H 
#define CD12_CODEGEN_INSTRUCTIONOUTCHUNK_H
#include "OutChunk.h"
#include "Instruction.h"

namespace cd12 {
namespace codegen {

class InstructionOutChunk : public OutChunk {
public:
	InstructionOutChunk(ins::Instruction ins);
	virtual ~InstructionOutChunk();

	virtual void generate(std::ostream& out = std::cout);
private:
	ins::Instruction m_instruction;
};

}
}
#endif
