#include "SM12CodeGenerator.h"

using namespace cd12;
using namespace codegen;

SM12CodeGenerator::SM12CodeGenerator(OutputSerializer& output) :
	m_out(output) {
	// TODO Auto-generated constructor stub
	m_registerCounter[0] = 0;
	m_registerCounter[1] = 0;
	m_registerCounter[2] = 0;
}

SM12CodeGenerator::~SM12CodeGenerator() {
	// TODO Auto-generated destructor stub
}

void SM12CodeGenerator::generate(parser::Node* root) {
	gdecs(root->child(0));
	subs(root->child(1));
	main(root->child(2));
}

void SM12CodeGenerator::gdecs(parser::Node* node) {
	for(auto child = node->begin(); child != node->end(); ++child) {
		if((*child)->id() == parser::NCDECS) { cdecs(*child); }
		if((*child)->id() == parser::NVDECS) { vdecs(*child); }
	}
}

void SM12CodeGenerator::cdecs(parser::Node* node) {
	for(auto child = node->begin(); child != node->end(); ++child) {
		if((*child)->id() == parser::NCDECS) { cdecs(*child); }
		else { m_out.addNamedConstant(*child); }
	}
}

void SM12CodeGenerator::vdecs(parser::Node* node) {
	// Globals go in base register 1
	for(auto child = node->begin(); child != node->end(); ++child) {
		if((*child)->id() == parser::NVDECS) { vdecs(*child); }
		else if((*child)->id() == parser::NSIMD) { m_out.addGlobal(*child); }
		else if((*child)->id() == parser::NARRD) {
			m_out.addGlobal(*child);
			m_out.addInstruction(ins::LA, (*child)->record());
			m_out.pushConstant((*child)->type());
			m_out.addInstruction(ins::ARRAY);
		}
	}
}

void SM12CodeGenerator::subs(parser::Node* node) {
	// TODO
}

void SM12CodeGenerator::func(parser::Node* node) {
	// TODO
}

void SM12CodeGenerator::procs(parser::Node* node) {
	// TODO
}

void SM12CodeGenerator::main(parser::Node* node) {
	for(auto child = node->begin(); child != node->end(); ++child) {
		if((*child)->id() == parser::NSLIST) { main(*child); }
		else { stat(*child); }
	}
}

void SM12CodeGenerator::stat(parser::Node* nstat) {
	switch(nstat->id()) {
	case parser::NEXIT: break;
	case parser::NASGN: asgnstat(nstat); break;
	case parser::NDISP:
	case parser::NNEWLN:
	case parser::NREAD:
		iostat(nstat);
		break;
	case parser::NCALL:
		//callstat(nstat);
		break;
	case parser::NEXPT:
		//exportstat(nstat);
		break;
	case parser::NIFTE:
	case parser::NIFT:
		ifstat(nstat);
		break;
	}
}

void SM12CodeGenerator::ifstat(parser::Node* nifst) {
	if(nifst->id() == parser::NIFTE) {
		int elsebranch = m_out.addForwardBranch();

		expr(nifst->child(0));
		m_out.addInstruction(ins::BF);

		// True case
		main(nifst->child(1));
		int truebranch = m_out.addForwardBranch();
		m_out.addInstruction(ins::BR);

		// False case
		m_out.resolveForward(elsebranch);
		main(nifst->child(2));

		// Jump here if no else
		m_out.resolveForward(truebranch);

	} else if(nifst->id() == parser::NIFT) {
		//m_out.addInstruction(ins::BT)
	}
}

void SM12CodeGenerator::asgnstat(parser::Node* nasgn) {
	var(nasgn->child(0), ins::LA);
	expr(nasgn->child(1));
	m_out.addInstruction(ins::ST);
}

void SM12CodeGenerator::iostat(parser::Node* nio) {
	if(nio->id() == parser::NDISP) {
		display(nio->child(0));
	} else if(nio->id() == parser::NNEWLN) {
		m_out.addInstruction(ins::NEWLN);
	} else if(nio->id() == parser::NREAD) {
		readlist(nio);
	}
}

void SM12CodeGenerator::display(parser::Node* ndisp) {
	for(auto child = ndisp->begin(); child != ndisp->end(); ++child)  {
		if((*child)->id() == parser::NPRLST) { display(*child); }
		else {
			expr(*child);
			if((*child)->id() == parser::NARRV) {
				m_out.addInstruction(ins::L);
			}
			m_out.addInstruction(ins::VALPR);
		}
	}
}

void SM12CodeGenerator::readlist(parser::Node* nlist) {
	for(auto child = nlist->begin(); child != nlist->end(); ++child) {
		if((*child)->id() == parser::NVLIST) { readlist(*child); }
		else {
			var(*child, ins::LA);
			m_out.addInstruction(ins::READF);
			m_out.addInstruction(ins::ST);
		}
	}
}

void SM12CodeGenerator::var(parser::Node* nvar, ins::SymbolInstruction in) {
	if(nvar->id() == parser::NSIMV) {
		m_out.addInstruction(in, nvar->record());
	} else if(nvar->id() == parser::NARRV) {
		m_out.addInstruction(ins::LV, nvar->record());
		expr(nvar->child(0));
		m_out.addInstruction(ins::INDEX);
	}
}

void SM12CodeGenerator::expr(parser::Node* nexpr) {
	// NADD, NSUB
	// NMUL, NDIV
	// NPOW
	// NFLIT
	// NLENG
	// NFNCL
	// NARRV, NSIMV

	// Do all the child stuff
	if(nexpr->hasChildren() && nexpr->id() != parser::NARRV) {
		expr(nexpr->child(0));
		expr(nexpr->child(1));
	}

	// Arithmetic checks
	std::map<parser::NodeID, ins::Instruction> map;
	map[parser::NADD] = ins::A;
	map[parser::NSUB] = ins::S;
	map[parser::NMUL] = ins::M;
	map[parser::NDIV] = ins::D;
	map[parser::NPOW] = ins::POW;
	map[parser::NNOT] = ins::CHS;


	if(map.find(nexpr->id()) != map.end()) {
		m_out.addInstruction(map[nexpr->id()]);
	}

	// Boolean checks
	std::map<parser::NodeID, ins::Instruction> boolean;
	boolean[parser::NGTR] = ins::GT;
	boolean[parser::NGEQ] = ins::GE;
	boolean[parser::NLSS] = ins::LT;
	boolean[parser::NLEQ] = ins::LE;
	boolean[parser::NAEQ] = ins::EQ;
	boolean[parser::NNEQ] = ins::NE;

	if(boolean.find(nexpr->id()) != boolean.end()) {
		m_out.addInstruction(boolean[nexpr->id()]);
	}

	if(nexpr->id() == parser::NFLIT) {
		//m_out.addInstruction(ins::LV, nexpr->record());
		m_out.pushConstant(nexpr->record());
		//m_out.addInstruction(ins::LES, scanner::Utility::lexical_cast<int>(nexpr->record()->getID()));
		//m_out.addInstruction(ins::LV, nexpr->record());
	}

	if(nexpr->id() == parser::NARRV || nexpr->id() == parser::NSIMV) {
		var(nexpr, ins::LV);
	}
}
