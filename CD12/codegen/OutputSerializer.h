#ifndef CD12_CODEGEN_OUTPUTSERIALIZER_H
#define CD12_CODEGEN_OUTPUTSERIALIZER_H
#include <iostream>
#include <vector>
#include <cmath>
#include <sstream>
#include <iterator>
#include <string>
#include "../parser/Node.h"
#include "../scanner/utility/Utility.h"
#include "Instruction.h"
#include "InstructionOutChunk.h"
#include "ArgInstructionOutChunk.h"
#include "SymbolInstructionOutChunk.h"
#include "ConstantInstructionOutChunk.h"
#include "ForwardBranchOutChunk.h"

namespace cd12 {
namespace codegen {

	class OutputSerializer {
	public:
		OutputSerializer();
		~OutputSerializer();

		void generate();

		void addGlobal(parser::Node* node);
		void addNamedConstant(parser::Node* node);
		void addSub(parser::Node* node);
		void pushConstant(symboltable::Record* record);
		void addInstruction(ins::Instruction ins);
		void addInstruction(ins::ArgInstruction ins, int argument);
		void addInstruction(ins::SymbolInstruction, symboltable::Record* symbol);

		int addForwardBranch();
		void resolveForward(int id);
	private:
		int m_constantCount;
		int m_globalCount;
		int m_insCount;

		std::vector<std::string> m_constantInsertionOrder;
		std::map<std::string, int> m_constants;
		std::map<std::string, std::string> m_namedConstants;

		int m_currentForward;
		std::map<int, ForwardBranchOutChunk*> m_forward; // <id, offset>

		std::vector<OutChunk*> m_chunks;
	};

}
}
#endif
