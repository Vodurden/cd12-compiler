#include "ForwardBranchOutChunk.h"

using namespace cd12;
using namespace codegen;

ForwardBranchOutChunk::ForwardBranchOutChunk(int id) :
		m_id(id),
		m_offset(-1){
}

ForwardBranchOutChunk::~ForwardBranchOutChunk() {

}

void ForwardBranchOutChunk::resolve(int insCount) {
	m_offset = insCount;
}


void ForwardBranchOutChunk::generate(std::ostream& out) {
	// LA0 address
	// Split m_base + m_offset into 4 255 byte chunks
	int fullOffset = m_offset;
	int offset[4];
	offset[0] = (fullOffset & 0xFF000000) >> 24;
	offset[1] = (fullOffset & 0x00FF0000) >> 16;
	offset[2] = (fullOffset & 0x0000FF00) >> 8;
	offset[3] = (fullOffset & 0x000000FF);

	// LV0 (m_base + m_offset)
	out << ins::LA << "0 " << offset[0] << " " << offset[1] << " " << offset[2] << " " << offset[3] << " ";
}

int ForwardBranchOutChunk::getOffset() {
	return m_offset;
}
