#include "OutputSerializer.h"

using namespace cd12;
using namespace codegen;

OutputSerializer::OutputSerializer() :
	m_constantCount(0),
	m_globalCount(0),
	m_insCount(0),
	m_currentForward(0) {

}

OutputSerializer::~OutputSerializer() {
	// TODO Auto-generated destructor stub
}

void OutputSerializer::generate() {
	std::stringstream out;
	std::stringstream instructions;

	int frontPadding = 0;

	// Work out how many instructions we're going to need
	m_insCount += 3; // 3 for the address setup
	m_insCount += 1; // 1 for the final newline
	if((m_insCount % 8) == 0) {
		// perfect fit number of instructions
		// means we need another word to fit
		// in a halt instruction
		m_insCount += 1;
	}

	// Generate the leftover padding amount needed
	int padAmount = 8;
	if((m_insCount % 8) != 0) {
		padAmount = 8 - (m_insCount % 8);
	}

	// Update all the chunks with the new information for branches and whatnot
	for(auto chunk = m_chunks.begin(); chunk != m_chunks.end(); ++chunk) {
		std::cout << "Update amount: " << m_insCount + padAmount << std::endl;
		(*chunk)->update(m_insCount + padAmount);
	}

	// Update all the forward branches with the new padding
	for(auto branch = m_forward.begin(); branch != m_forward.end(); ++branch) {
		branch->second->resolve(branch->second->getOffset() + 3);
	}

	// Generate space for all the globals
	instructions << ins::LES << " " << m_globalCount << " " << ins::ALLOC << " ";

	// Generate all the instructions
	for(auto chunk = m_chunks.begin(); chunk != m_chunks.end(); ++chunk) {
		(*chunk)->generate(instructions);
	}

	instructions << ins::NEWLN << " ";


	for(int i = 0; i < padAmount; ++i) {
		instructions << "0 ";
	}

	// Set up the instruction space
	out << std::ceil(((float)m_insCount) / 8.0) << std::endl;

	auto begin = std::istream_iterator<std::string>(instructions);
	auto end = std::istream_iterator<std::string>();
	int count = 0 ;
	while(begin != end) {
		out << *begin << " ";
		++count;
		if(count >= 8) {
			count = 0;
			out << std::endl;
		}
		++begin;
	}


	//out << instructions.str();

	// Generate the constants
	std::stringstream constants;
	constants << m_constants.size() << std::endl;
	for(auto cons = m_constantInsertionOrder.begin(); cons != m_constantInsertionOrder.end(); ++cons) {
		constants << *cons<< std::endl;
	}

	out << constants.str();

	// Generate the strings
	out << "0" << std::endl;

	std::cout << out.str();
}


void OutputSerializer::addGlobal(parser::Node* node) {
	node->record()->baseRegister(1);
	node->record()->memOffset(m_globalCount * 8);
	++m_globalCount;
}
void OutputSerializer::addNamedConstant(parser::Node* node) {
	m_namedConstants[node->record()->getID()] = node->child(0)->record()->getID();
	//m_namedConstants[node->child(0)->record()->getID()] = node->child(1)->record()->getID();
	/*node->record()->baseRegister(0);
	node->record()->memOffset((m_constantCount * 8));
	++m_constantCount;*/
}

void OutputSerializer::addSub(parser::Node* node) {

}

void OutputSerializer::addInstruction(ins::Instruction ins) {
	m_insCount += 1;
	m_chunks.push_back(new InstructionOutChunk(ins));
}

void OutputSerializer::addInstruction(ins::ArgInstruction ins, int argument) {
	m_insCount += 2;
	m_chunks.push_back(new ArgInstructionOutChunk(ins, argument));
	/*std::cout << ins << " " << argument << " ";
	if(m_insCount % 8 == 0) {
		std::cout << std::endl;
	}*/
}

void OutputSerializer::addInstruction(ins::SymbolInstruction ins, symboltable::Record* symbol) {
	m_insCount += 5;
	m_chunks.push_back(new SymbolInstructionOutChunk(ins, symbol));
}

int OutputSerializer::addForwardBranch() {
	// Assume a boolean condition has already been pushed on the stack
	ForwardBranchOutChunk* chunk = new ForwardBranchOutChunk(m_currentForward);
	m_forward[m_currentForward] = chunk;
	m_chunks.push_back(chunk);

	m_insCount += 5;

	return m_currentForward++;
}

void OutputSerializer::resolveForward(int id) {
	m_forward[id]->resolve(m_insCount);
}

void OutputSerializer::pushConstant(symboltable::Record* record) {
	std::string recID = record->getID();

	// Check if the record references a named constant
	if(m_namedConstants.find(record->getID()) != m_namedConstants.end()) {
		auto named = m_namedConstants[record->getID()];
		recID = named;
	}

	// Check if we've already allocated the constant, if so load i
	if(m_constants.find(recID) == m_constants.end()) {
		m_constants[recID] = (m_constantCount * 8);
		m_constantInsertionOrder.push_back(recID);
		++m_constantCount;
	}

	int final = m_constants[recID];
	m_chunks.push_back(new ConstantInstructionOutChunk(final));
	m_insCount += 5;
}
