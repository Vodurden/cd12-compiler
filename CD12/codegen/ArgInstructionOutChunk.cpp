#include "ArgInstructionOutChunk.h"

using namespace cd12;
using namespace codegen;

ArgInstructionOutChunk::ArgInstructionOutChunk(ins::ArgInstruction in, int argument) :
	m_instruction(in),
	m_argument(argument) {

}

ArgInstructionOutChunk::~ArgInstructionOutChunk() {

}

void ArgInstructionOutChunk::generate(std::ostream& out) {
	out << m_instruction << " " << m_argument << " ";
}
