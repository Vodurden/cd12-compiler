#ifndef CD12_CODEGEN_SM12CODEGENERATOR_H
#define CD12_CODEGEN_SM12CODEGENERATOR_H
#include <iostream>
#include <map>
#include <tr1/functional>
#include "../scanner/utility/Utility.h"
#include "../parser/NodeID.h"
#include "../parser/Node.h"
#include "OutputSerializer.h"
#include "Instruction.h"

namespace cd12 {
namespace codegen {

	class SM12CodeGenerator {
	public:
		SM12CodeGenerator(OutputSerializer& output);
		~SM12CodeGenerator();

		void generate(parser::Node* root);
	private:
		OutputSerializer& m_out;
		int m_registerCounter[3];

		void gdecs(parser::Node* node);
			void cdecs(parser::Node* node);
			void vdecs(parser::Node* node);
				void simd(parser::Node* node);
		void subs(parser::Node* node);
			void procs(parser::Node* node);
			void func(parser::Node* node);
		void main(parser::Node* node);
			void stat(parser::Node* nstat);
				void exitstat(parser::Node* node);
				void asgnstat(parser::Node* node);
				void iostat(parser::Node* node);
					void display(parser::Node* node);
					void readlist(parser::Node* nvar);
				void callstat(parser::Node* node);
				void exportstat(parser::Node* node);
				void ifstat(parser::Node* node);
					void boolstat(parser::Node* node);
			void var(parser::Node* nvar, ins::SymbolInstruction in);
			void expr(parser::Node* nexpr);

	};

	//NOTE: Base Register 0 - Constants
	//NOTE: Base Register 1 - Main Memory
	//NOTE: Base Register 2 - Functions

}
}
#endif
