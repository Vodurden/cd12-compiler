#ifndef CD12_CODEGEN_ARGINSTRUCTIONOUTCHUNK_H
#define CD12_CODEGEN_ARGINSTRUCTIONOUTCHUNK_H
#include "OutChunk.h"
#include "Instruction.h"
namespace cd12 {
namespace codegen {
	class ArgInstructionOutChunk : public OutChunk {
	public:
		ArgInstructionOutChunk(ins::ArgInstruction in, int argument);
		virtual ~ArgInstructionOutChunk();

		virtual void generate(std::ostream& out = std::cout);
	private:
		ins::ArgInstruction m_instruction;
		int m_argument;
	};
}
}
#endif
