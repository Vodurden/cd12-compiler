#include "SymbolInstructionOutChunk.h"

using namespace cd12;
using namespace codegen;

SymbolInstructionOutChunk::SymbolInstructionOutChunk(ins::SymbolInstruction in, symboltable::Record* record) :
	m_instruction(in),
	m_record(record) {

}

SymbolInstructionOutChunk::~SymbolInstructionOutChunk() {

}

void SymbolInstructionOutChunk::generate(std::ostream& out) {
	out << m_instruction << m_record->baseRegister() << " ";
	out << "0 0 0 " << m_record->memOffset() << " ";
}
