#ifndef CD12_CODEGEN_OUTCHUNK_H
#define CD12_CODEGEN_OUTCHUNK_H
#include <iostream>
namespace cd12 {
namespace codegen {
	class OutChunk {
	public:
		virtual ~OutChunk();
		virtual void update(int instructionWords);
		virtual void generate(std::ostream& out = std::cout) = 0;
	};
}
}
#endif
