#include "ConstantInstructionOutChunk.h"

using namespace cd12;
using namespace codegen;

ConstantInstructionOutChunk::ConstantInstructionOutChunk(int offset) :
	m_base(-10000),
	m_offset(offset) {
}

ConstantInstructionOutChunk::~ConstantInstructionOutChunk() {

}

void ConstantInstructionOutChunk::update(int instructionWords) {
	m_base = (instructionWords);
}

void ConstantInstructionOutChunk::generate(std::ostream& out) {
	// Split m_base + m_offset into 4 255 byte chunks
	std::cout << "base: " << m_base;
	std::cout << " off: " << m_offset;
	int fullOffset = (m_base + m_offset);
	std::cout << " fullOffset: " << fullOffset << std::endl;
	int offset[4];
	offset[0] = (fullOffset & 0xFF000000) >> 24;
	offset[1] = (fullOffset & 0x00FF0000) >> 16;
	offset[2] = (fullOffset & 0x0000FF00) >> 8;
	offset[3] = (fullOffset & 0x000000FF);

	// LV0 (m_base + m_offset)
	out << ins::LV << "0 " << offset[0] << " " << offset[1] << " " << offset[2] << " " << offset[3] << " ";
}
