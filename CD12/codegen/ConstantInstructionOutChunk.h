#ifndef CD12_CODEGEN_CONSTANTINSTRUCTIONOUTCHUNK_H
#define CD12_CODEGEN_CONSTANTINSTRUCTIONOUTCHUNK_H
#include <string>
#include "OutChunk.h"
#include "Instruction.h"
namespace cd12 {
namespace codegen {
	class ConstantInstructionOutChunk : public OutChunk {
	public:
		ConstantInstructionOutChunk(int offset);
		virtual ~ConstantInstructionOutChunk();

		virtual void update(int instructionWords);
		virtual void generate(std::ostream& out = std::cout);
	private:
		int m_base;
		int m_offset;
	};
}
}
#endif
