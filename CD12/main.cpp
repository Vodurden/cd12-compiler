#include <set>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <iterator>
#include <fstream>

// Symbol Table
#include "symboltable/SymbolTable.h"
#include "symboltable/SymbolTableManager.h"

// Lexical Analysis
#include "scanner/Scanner.h"
#include "scanner/TokenID.h"
#include "scanner/filter/UndefinedFilter.h"
#include "output/TokenPrinter.h"
#include "output/ParserGraphGenerator.h"

// Parser
#include "parser/Parser.h"
#include "parser/Node.h"

// Code Generation
#include "codegen/SM12CodeGenerator.h"
#include "codegen/OutputSerializer.h"

int main(int argc, char** argv) {
	//std::ifstream stream("test/parser/data/e_test1.cd12");

	// Read the input in
	/*std::cin >> std::noskipws;
	std::istreambuf_iterator<char> start(std::cin);
	std::istreambuf_iterator<char> end;
	std::string program(start, end);*/
	/*std::stringstream prog;
	prog
	<< "PROGRAM id constant arrayOne = 5, arrayTwo = 5, c = 5\n"
	<< "global varA, b, c, d[arrayOne], f[arrayTwo], asd[c]\n"
	<< "function myfunc(param1, *param2, param3[arrayOne], param4[]) { local a, b[arrayOne], asd, asdaskdasd; newline; newline; newline; }\n"
	<< "function interesting(*param2, par3) { local supdog; newline; newline; }\n"
	<< "function another() { local why; newline; export 5; export 5; }\n"
	<< "procedure pro() { local wat; export 5; export 5; export 5;}\n"
	<< "procedure paramTest(*something) { local s; display something; }\n"
	<< "main newline;\n"
	<< "call paramTest(5, 5);\n"
	<< "read varA, varB[5];\n"
	<< "display \"hello world\";\n"
	<< "exit abc when 5;\n"
	<< "call something (5, 10);\n"
	<< "call abc();\n"
	<< "export 5;\n"
	<< "someID = 5;\n"
	<< "someUndeclaredVariable = 5;\n"
	<< "arrayOne = arrayOne.length;\n"
	<< "arrayOne = arrayOne();\n"
	<< "m = myfunc.length;\n"
	<< "with i = 0, j = 0 loop dostuff { export 5; exit dostuff when 5 ~= 5;}\n"
	<< "with i = 0 loop noexit { export 125; }\n"
	<< "export 100;\n"
	<< "if 5 > 100 and 3 < 2 + 3 + 4 or 500 > 1000 { export 50; }\n"
	<< "if not 5 > 100 and 3 < 2 + 3 + 4 or 500 > 1000 { export 50; }\n"
	<< "display 5 + 3 + 2 - 10 - 50 - 100;\n"
	<< "display 13 - 14 - 15;\n"
	<< "display 13 - (14 - 15);\n"
	<< "end id";
	std::string program = prog.str();*/

	std::stringstream prog;
	prog
	<< "program test\n"
	<< "global x, y\n"
	<< "main\n"
	<< " x = 5;\n"
	<< " y = 10;\n"
	<< " if ( x > y ) {\n"
	<< "   display x;\n"
	<< " } else {\n"
	<< "   display y;\n"
	<< " }\n"
	<< "end test";
	std::string program = prog.str();

	/*prog
	<< "PROGRAM good\n"
	<< "constant size = 100\n"
	<< "global a, b, c, array[size]\n"
	<< "function fn() { export 5; }\n"
	<< "main\n"
	<< "    display 15;"
	<< "end good";
	std::string program(prog.str());*/

	if(program.empty()) {
		std::cerr << "No input data. Terminating." << std::endl;
	}

	// Symboltable Setup
	cd12::symboltable::SymbolTableManager tableManager;

	// Lexical Analysis
	cd12::scanner::Scanner scanner(program, tableManager);
	cd12::scanner::filter::UndefinedFilter undfFilter(scanner);

	// Syntax Tree Generation
	cd12::parser::Parser parser(undfFilter, tableManager);

	/*cd12::output::TokenPrinter printer(scanner);
	printer.print();*/

	cd12::parser::Node* programNode = parser.parse();
	if(!parser.fatalError()) {
		cd12::output::ParserGraphGenerator graphGenerator("html_output", programNode); 
		graphGenerator.generate("syntax_tree.html");
	} else {
		std::cout << "Parse tree not generated due to fatal error" << std::endl;
	}

	// Code Generation
	cd12::codegen::OutputSerializer out;
	cd12::codegen::SM12CodeGenerator codegen(out);
	codegen.generate(programNode);
	out.generate();
}
