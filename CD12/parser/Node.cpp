#include "Node.h"

namespace cd12 {
namespace parser {

Node::Node() :
	m_id(NODEID_ENUM_INVALID),
	m_record(0),
	m_type(0) {

}

Node::Node(NodeID id) :
	m_id(id),
	m_record(0),
	m_type(0) {

}

Node::~Node() {
	// TODO Auto-generated destructor stub
}

NodeID Node::id() const {
	return m_id;
}

bool Node::hasChildren() const {
	return !m_children.empty();
}

symboltable::Record* Node::record() const {
	return m_record;
}

void Node::record(symboltable::Record* record) {
	m_record = record;
}

symboltable::Record* Node::type() const {
	return m_type;
}

void Node::type(symboltable::Record* type) {
	m_type = type;
}
std::list<Node*>::iterator Node::begin() {
	return m_children.begin();
}

std::list<Node*>::iterator Node::end() {
	return m_children.end();
}

Node* Node::child(int index) {
	auto listit = m_children.begin();
	std::advance(listit, index);
	return *listit;
}

Node* Node::operator[](int index) {
	return child(index);
}

void Node::addChild(Node* child) {
	if(child == 0) {
		return;
	}

	if(child->m_id == NODEID_ENUM_INVALID) {
		// TODO: Consider delelting child here
		for(auto c = child->m_children.begin(); c != child->m_children.end(); ++c) {
			m_children.push_back(*c);
		}
	} else {
		m_children.push_back(child);
	}
}

void Node::addChildFront(Node* child) {
	if(child == 0) {
		return;
	}

	if(child->m_id == NODEID_ENUM_INVALID) {
		// TODO: Consider delelting child here
		for(auto c = child->m_children.begin(); c != child->m_children.end(); ++c) {
			m_children.push_front(*c);
		}
	} else {
		m_children.push_front(child);
	}
}

}
}
