#ifndef CD12_PARSER_NODE_H 
#define CD12_PARSER_NODE_H
#include <list>
#include "NodeID.h"
#include "../symboltable/Record.h"
namespace cd12 {
namespace parser {

	class Node {
		public:
			Node();
			Node(NodeID id);
			virtual ~Node();

			NodeID id() const;

			bool hasChildren() const;

			symboltable::Record* record() const;
			void record(symboltable::Record* record);

			symboltable::Record* type() const;
			void type(symboltable::Record* type);

			std::list<Node*>::iterator begin();
			std::list<Node*>::iterator end();

			Node* child(int index);
			Node* operator[](int index);

			void addChild(Node* child);
			void addChildFront(Node* child);
		private:
			NodeID m_id;
			symboltable::Record* m_record;
			symboltable::Record* m_type;
			std::list<Node*> m_children;
	};

}
}
#endif
