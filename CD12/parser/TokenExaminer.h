/*
 * TokenExaminer.h
 *
 *  Created on: Sep 9, 2012
 *      Author: vodurden
 */

#ifndef CD12_PARSER_TOKENEXAMINER_H 
#define CD12_PARSER_TOKENEXAMINER_H
#include "../scanner/IScanner.h"
#include "../scanner/Token.h"

namespace cd12 {
namespace parser {

	class TokenExaminer {
		public:
			TokenExaminer(scanner::IScanner& scanner);
			virtual ~TokenExaminer();

			scanner::Token* next(); // Move to the next token and return it

			scanner::Token* get(); // Get the current token
			scanner::Token* peek(); // Get the next token without moving

			bool hasNext();
		private:
			scanner::IScanner& m_scanner;
			scanner::Token* m_current;
			scanner::Token* m_next;
	};

}
}
#endif
