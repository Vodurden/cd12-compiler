#ifndef CD12_PARSER_PARSER_H
#define CD12_PARSER_PARSER_H
#include <iostream>
#include <vector>
#include <tr1/functional>
#include "../scanner/IScanner.h"
#include "../scanner/Token.h"
#include "../scanner/TokenID.h"

#include "TokenExaminer.h"

#include "NodeID.h"
#include "Node.h"
#include "EnforcementData.h"
#include "exception/RequirementFailureException.h"
#include "exception/RequirementNoSyncException.h"
#include "../symboltable/SymbolTableManager.h"

namespace cd12 {
	namespace parser {

		class Parser {
			public:
				Parser(scanner::IScanner& scanner, symboltable::SymbolTableManager& symbolManager);
				virtual ~Parser();

				Node* parse();

				bool fatalError();
				bool nonFatalError();
			private:
				bool m_fatalError;
				bool m_nonFatalError;

				scanner::IScanner& m_scanner;
				TokenExaminer m_examiner;
				std::vector<scanner::TokenID> m_syncs;

				symboltable::SymbolTableManager m_symbolManager;

				bool m_isEnforcing;
				std::map<scanner::TokenID, EnforcementData> m_minEnforcement;
				std::map<scanner::TokenID, EnforcementData> m_maxEnforcement;

				// Semantic Checking
				void enforceMinimum(scanner::TokenID id, int minimum, scanner::Token* enforceScope, const std::string& message);
				void enforceMaximum(scanner::TokenID id, int maxiumum, scanner::Token* enforceScope, const std::string& message);
				void updateEnforcement(scanner::TokenID id);
				void endEnforcement(scanner::TokenID id);

				symboltable::Record* semantic(scanner::Token* token,
					std::vector<std::tr1::function<bool (symboltable::Record*, symboltable::SymbolTableManager&)> > validate,
					const std::string& error);

				// Utility functions
				scanner::Token* expect(scanner::TokenID id);
				scanner::Token* require(scanner::TokenID id);
				scanner::Token* maybe(scanner::TokenID id);
				bool synchronizedAdd(Node* node,
						std::tr1::function<Node* ()> childFunc,
						std::vector<scanner::TokenID> syncTokens,
						std::vector<scanner::TokenID> killTokens = std::vector<scanner::TokenID>(),
						bool consumeSyncToken = true);

				void addSync(scanner::TokenID id);
				void clearSync(scanner::TokenID id);
				void clearSyncs();
				void debugMsg(const std::string& msg);

				Node* id();

				// Recursive Descent Functions
				// Program Mains
				Node* program();
				Node* gdecls();
					Node* consts();
						Node* initlist();
						Node* initlisttail();
						Node* init();
					Node* globals();
						Node* vardecls();
						Node* vardeclstail();
						Node* vardecl();
				Node* subs();
					Node* sub();
						Node* func();
						Node* proc();
							Node* procmiddle();
							Node* proctail();
							Node* procbody();
						Node* plist();
							Node* params();
							Node* param();
						Node* locals();
				Node* stats();
					Node* stat();
						Node* exitstat();
						Node* asgnstat();
						Node* iostat();
						Node* callstat();
						Node* exportstat();
					Node* bigstat();
						Node* ifstat();
							Node* asgnlist();
						Node* loopstat();
				Node* boolstat();
					Node* rel();
					Node* booltail(Node* subtree);
					Node* reltail(Node* subtree);
					Node* logop();
					Node* relop();
				Node* vlist();
					Node* var();
					Node* varimpl(scanner::Token* tok);
				Node* prlist();
					Node* printitem();
				Node* elist();
					Node* expr();
						Node* etail(Node* subtree);
						Node* term();
						Node* ttail(Node* subtree);
						Node* fact();
						Node* ftail(Node* subtree);
						Node* exponent();
						Node* fncall();
						Node* fncalltail();
		};

	}
}
#endif
