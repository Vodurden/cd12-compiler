/*
 * RequirementFailureException.h
 *
 *  Created on: Sep 12, 2012
 *      Author: vodurden
 */

#ifndef CD12_PARSER_EXCEPTION_REQUIREMENTFAILUREEXCEPTION_H 
#define CD12_PARSER_EXCEPTION_REQUIREMENTFAILUREEXCEPTION_H
#include <typeinfo>
#include "../../scanner/TokenID.h"

namespace cd12 {
namespace parser {
namespace exception {

	class RequirementFailureException : public std::exception {
		public:
			RequirementFailureException(scanner::TokenID syncID);

			scanner::TokenID syncID();
		private:
			scanner::TokenID m_syncID;
	};

}
}
}
#endif
