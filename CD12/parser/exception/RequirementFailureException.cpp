/*
 * RequirementFailureException.cpp
 *
 *  Created on: Sep 12, 2012
 *      Author: vodurden
 */

#include "RequirementFailureException.h"

namespace cd12 {
namespace parser {
namespace exception {


RequirementFailureException::RequirementFailureException(scanner::TokenID syncID) :
	std::exception(),
	m_syncID(syncID){
}

scanner::TokenID RequirementFailureException::syncID() {
	return m_syncID;
}

}
}
}
