/*
 * RequirementNoSyncException.h
 *
 *  Created on: Sep 12, 2012
 *      Author: vodurden
 */

#ifndef CD12_PARSER_EXCEPTION_REQUIREMENTNOSYNCEXCEPTION_H
#define CD12_PARSER_EXCEPTION_REQUIREMENTNOSYNCEXCEPTION_H
#include <typeinfo>

namespace cd12 {
namespace parser {
namespace exception {

	class RequirementNoSyncException : public std::exception {
		public:
			RequirementNoSyncException();
	};

}
}
}
#endif
