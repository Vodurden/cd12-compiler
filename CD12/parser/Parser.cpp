/*
 * Parser.cpp
 *
 *  Created on: Sep 6, 2012
 *      Author: vodurden
 */

#include "Parser.h"

using namespace cd12;
using namespace parser;

Parser::Parser(scanner::IScanner& scanner, symboltable::SymbolTableManager& symbolManager) :
		m_fatalError(false),
		m_nonFatalError(false),
		m_scanner(scanner),
		m_examiner(m_scanner),
		m_symbolManager(symbolManager),
		m_isEnforcing(false) {
}

Parser::~Parser() {
}

Node* Parser::parse() {
	return program();
}

bool Parser::fatalError() {
	return m_fatalError;
}

bool Parser::nonFatalError() {
	return m_nonFatalError;
}

// Utility
scanner::Token* Parser::expect(scanner::TokenID id) {
	if(!m_examiner.hasNext()) {
		return 0;
	}

	scanner::Token* tok = m_examiner.peek();
	if(tok != 0 && tok->id() == id) {
		// Check the minimum and maximum enforcements if they're being used
		updateEnforcement(tok->id());

		m_examiner.next();
		return tok;
	}

	return 0;
}

scanner::Token* Parser::maybe(scanner::TokenID id) {
	if(!m_examiner.hasNext()) {
		return 0;
	}

	scanner::Token* tok = m_examiner.peek();
	if(tok != 0 && tok->id() == id) {
		return tok;
	}

	return 0;
}

scanner::Token* Parser::require(scanner::TokenID id) {
	if(!m_examiner.hasNext()) {
		return 0;
	}

	scanner::Token* tok = m_examiner.peek();
	scanner::Token* originalTok = tok;
	if(tok && tok->id() == id) {
		updateEnforcement(tok->id());

		m_examiner.next();
		return tok;
	}

	bool found = false;
	scanner::Token* syncTok = 0;

	// Check if the current token is a resync token
	if(std::find(m_syncs.begin(), m_syncs.end(), tok->id()) != m_syncs.end()) {
		found = true;
		syncTok = tok;
	}

	// From this point on we're doing error handling, so panic and attempt resynchronization
	while(!found && m_examiner.hasNext()) {
		tok = m_examiner.peek();
		if(std::find(m_syncs.begin(), m_syncs.end(), tok->id()) != m_syncs.end()) {
			found = true;
			syncTok = tok;
			break;
		} else {
			tok = m_examiner.next();
		}
	}

	// Rescynchronize
	std::stringstream error;
	if(!found) {
		error << "Error: At [line:col] " << originalTok->row() << ":" << originalTok->col() << " ";
		error << "Expected " << cd12::scanner::Utility::getTokenName(id) << " found " << cd12::scanner::Utility::getTokenName(originalTok->id()) << std::endl;
		std::cerr << error.str();
		throw exception::RequirementNoSyncException();
	} else {
		error << "Error: At [line:col] " << originalTok->row() << ":" << originalTok->col() << " ";
		error << "Expected " << cd12::scanner::Utility::getTokenName(id) << " found " << cd12::scanner::Utility::getTokenName(originalTok->id()) << std::endl;// << " with sync";
		//error << " on " << cd12::scanner::Utility::getTokenName(tok->id()) << " at " << tok->row() << ":" << tok->col() << std::endl;
		std::cerr << error.str();
		m_nonFatalError = true;
		throw exception::RequirementFailureException(syncTok->id());
	}


	return 0;
}

bool Parser::synchronizedAdd(Node* node,
		std::tr1::function<Node* ()> childFunc,
		std::vector<scanner::TokenID> syncTokens,
		std::vector<scanner::TokenID> killTokens,
		bool consumeSyncToken) {
	bool addedChild = false;
	// add all the syncTokens to m_syncs
	for(auto tokID = syncTokens.begin(); tokID != syncTokens.end(); ++tokID){
		addSync(*tokID);
	}

	for(auto tokID = killTokens.begin(); tokID != killTokens.end(); ++tokID) {
		addSync(*tokID);
	}

	while(!addedChild) {
		try {
			node->addChild(childFunc());

			// If addChild throws an exception we won't hit this point
			addedChild = true;
		} catch(exception::RequirementFailureException& ex) {
			// Kill the search if we've hit a kill token
			if(std::find(killTokens.begin(), killTokens.end(), ex.syncID()) != killTokens.end()) {
				debugMsg("Killing sync on token: " + ex.syncID());
				return false;
			}

			// Try and find the syncToken in our handleable tokens if it fails we need to rethrow
			if(std::find(syncTokens.begin(), syncTokens.end(), ex.syncID()) == syncTokens.end()) {
				debugMsg("Unable to handle sync, throwing: " + ex.syncID());
				throw ex;
			} else {
				if(consumeSyncToken) {
					m_examiner.next();
				}
			}
		}
	}

	for(auto tokID = syncTokens.begin(); tokID != syncTokens.end(); ++tokID){
		clearSync(*tokID);
	}

	for(auto tokID = killTokens.begin(); tokID != killTokens.end(); ++tokID) {
		clearSync(*tokID);
	}

	return true;
}

// ==================== SEMANTICS ===========================
std::string bindvar(symboltable::Record* record, const std::string& error) {
	std::string output(error);

	std::map<std::string, std::string> replacements;
	replacements["var"] = record->getID();
	replacements["type"] = symboltable::toString(record->getType());

	// Replace all occurrences of {{var}} with the variable name
	for(auto pair = replacements.begin(); pair != replacements.end(); ++pair) {
		std::size_t index;
		while(true) {
			index = output.find("{{" + pair->first + "}}");
			if(index == std::string::npos) break;

			output.replace(index, pair->first.length() + 4, pair->second);

			++index;
		}
	}

	return output;
}

void Parser::enforceMinimum(scanner::TokenID id, int minimum, scanner::Token* enforceScope, const std::string& errorMessage) {
	m_isEnforcing = true;
	m_minEnforcement[id] = EnforcementData(0, minimum, enforceScope, errorMessage);
}

void Parser::enforceMaximum(scanner::TokenID id, int maximum, scanner::Token* enforceScope, const std::string& errorMessage) {
	m_isEnforcing = true;
	m_maxEnforcement[id] = EnforcementData(0, maximum, enforceScope, errorMessage);
}

void Parser::updateEnforcement(scanner::TokenID id) {
	if(m_isEnforcing) {
		if(m_minEnforcement.find(id) != m_minEnforcement.end()) {
			++(m_minEnforcement[id].count);
		}

		if(m_maxEnforcement.find(id) != m_maxEnforcement.end()) {
			++(m_maxEnforcement[id].count);
		}
	}
}

void Parser::endEnforcement(scanner::TokenID id) {
	// Go through each enforcement and find the ID, then check if it's erronous
	auto min = m_minEnforcement.find(id);
	if(min != m_minEnforcement.end()) {
		EnforcementData data = min->second;
		if(data.count < data.min) {
			std::cerr << "semantic error: [line:col] [" << data.scope->row() << ":" << data.scope->col() << "] "
				<< bindvar(data.scope->record(), data.errorMessage) << std::endl;
		}
	}

	auto max = m_maxEnforcement.find(id);
	if(max != m_maxEnforcement.end()) {
		EnforcementData data = max->second;
		if(data.count > data.max) {
			std::cerr << "semantic error: [line:col] [" << data.scope->row() << ":" << data.scope->col() << "] "
					<< bindvar(data.scope->record(), data.errorMessage) << std::endl;
		}
	}

	m_isEnforcing = false;
}



symboltable::Record* Parser::semantic(scanner::Token* token,
		std::vector<std::tr1::function<bool (symboltable::Record*, symboltable::SymbolTableManager& manager)> > validators,
		const std::string& error) {

	// Look up the real record
	symboltable::Record* record = m_symbolManager.lookup(token->record()->getID());
	if(!record) { // All semantic checking requires a defined record.
		std::cerr << "semantic error: [line:col] [" << token->row() << ":" << token->col() << "] "
				<< "attempt to use undefined symbol " << token->record()->getID() << std::endl;
		return token->record();
	}

	// Execute each validator on the token, if any are false return 0
	for(auto validator = validators.begin(); validator != validators.end(); ++validator) {
		if(!(*validator)(record, m_symbolManager)) {
			std::cerr << "semantic error: [line:col] [" << token->row() << ":" << token->col() << "] "
				<< bindvar(record, error) << std::endl;
			return token->record();
		}
	}

	return record;
}


bool isArray(symboltable::Record* record, symboltable::SymbolTableManager& manager) {
	return (record->getType() == symboltable::TYPE_ARRAY_BOUNDED || record->getType() == symboltable::TYPE_ARRAY_UNBOUNDED);
}

bool isFunction(symboltable::Record* record, symboltable::SymbolTableManager& manager) {
	return (record->getType() == symboltable::TYPE_FUNCTION);
}

bool isProcedure(symboltable::Record* record, symboltable::SymbolTableManager& manager) {
	return (record->getType() == symboltable::TYPE_PROCEDURE);
}

bool isCallable(symboltable::Record* record, symboltable::SymbolTableManager& manager) {
	return (isFunction(record, manager) || isProcedure(record, manager));
}

bool isSimple(symboltable::Record* record, symboltable::SymbolTableManager& manager) {
	return (record->getType() == symboltable::TYPE_FLOAT || record->getType() == symboltable::TYPE_REF_PARAM);
}

// ==========================================================

void Parser::debugMsg(const std::string& msg) {
	//std::cerr << msg << std::endl;
}

void Parser::addSync(scanner::TokenID id) {
	if(std::find(m_syncs.begin(), m_syncs.end(), id) == m_syncs.end()) {
		m_syncs.push_back(id);
	}
}

void Parser::clearSync(scanner::TokenID id) {
	m_syncs.erase(std::remove(m_syncs.begin(), m_syncs.end(), id), m_syncs.end());
}

void Parser::clearSyncs() {
	m_syncs.clear();
}

// Recursive Descent
Node* Parser::program() {
	// Expects the PROGRAM keyword first
	Node* program = 0;
	try {
		require(scanner::TPROG);
		scanner::Token* firstID = require(scanner::TID);

		program = new Node(NPROG);
		//program->record(firstID->name());
		program->addChild(gdecls());
		program->addChild(subs());
		require(scanner::TMAIN);
		enforceMaximum(scanner::TEXPT, 0, firstID, "main cannot contain an export statement");
			program->addChild(stats());
		endEnforcement(scanner::TEXPT);
		require(scanner::TEND);

		scanner::Token* secondID = require(scanner::TID);

		if(firstID->record()->getID() != secondID->record()->getID()) {
			// TODO: SEMANTIC ERROR
		}
	} catch(exception::RequirementFailureException& ex) {
		std::cerr << "Top level requirement failure exception FATAL" << std::endl;
		std::cerr << "Top level no sync FATAL" << std::endl;
		m_fatalError = true;
	} catch(exception::RequirementNoSyncException& ex) {
		std::cerr << "Top level no sync FATAL" << std::endl;
		m_fatalError = true;
	}
	return program;
}

Node* Parser::gdecls() {
	Node* gdecls = new Node(NGDECS);
	gdecls->addChild(consts());
	gdecls->addChild(globals());
	return gdecls;
}

Node* Parser::consts() {
	if(!expect(scanner::TCONS)) {
		return 0;
	}

	debugMsg("Doing Constants");
	return initlist();
}

Node* Parser::initlist() {
	Node* cdecs = new Node(NCDECS);
	if(!synchronizedAdd(cdecs, std::tr1::bind(&Parser::init, this), {scanner::TCOMA}, {scanner::TGLOB, scanner::TMAIN, scanner::TPROC, scanner::TFUNC})) {
		return 0;
	}

	//initlisttail
	if(maybe(scanner::TGLOB) || maybe(scanner::TPROC) || maybe(scanner::TFUNC) || maybe(scanner::TMAIN)) {
		return cdecs;
	}

	if(!synchronizedAdd(cdecs, std::tr1::bind(&Parser::initlisttail, this), {scanner::TCOMA}, {scanner::TGLOB, scanner::TMAIN, scanner::TPROC, scanner::TFUNC})) {
		return cdecs;
	}

	return cdecs;
}

Node* Parser::initlisttail() {
	require(scanner::TCOMA);
	return initlist();
}

Node* Parser::init() {
	// <id> = <expr>
	scanner::Token* id = require(scanner::TID);
	require(scanner::TASGN);

	id->record()->setType(symboltable::TYPE_FLOAT);

	// If we're here we know the require's succeeded
	Node* init = new Node(NCONST);
	init->record(m_symbolManager.addRecord(id->record()));
	init->addChild(expr());

	return init;
}

Node* Parser::globals() {
	if(!expect(scanner::TGLOB)) {
		return 0;
	}

	debugMsg("Doing globals");
	return vardecls();
}

Node* Parser::vardecls() {
	Node* vdecs = new Node(NVDECS);
	if(!synchronizedAdd(vdecs, std::tr1::bind(&Parser::vardecl, this), {scanner::TCOMA}, {scanner::TMAIN, scanner::TFUNC, scanner::TPROC})) {
		return 0;
	}

	// vardeclstail
	if(maybe(scanner::TPROC) || maybe(scanner::TFUNC) || maybe(scanner::TMAIN) || maybe(scanner::TSEMI)) {
		return vdecs;
	}

	if(!synchronizedAdd(vdecs, std::tr1::bind(&Parser::vardeclstail, this), {scanner::TCOMA}, {scanner::TMAIN, scanner::TPROC, scanner::TFUNC})) {
		return vdecs;
	}

	return vdecs;
}

Node* Parser::vardeclstail() {
	require(scanner::TCOMA);
	return vardecls();
}

Node* Parser::vardecl() {
	scanner::Token* id = require(scanner::TID);

	if(expect(scanner::TLBRK)) {

		// NARRD
		scanner::Token* constant = require(scanner::TID);
		require(scanner::TRBRK);

		id->record()->setType(symboltable::TYPE_ARRAY_BOUNDED);
		constant->record()->setType(symboltable::TYPE_FLOAT);

		Node* narrd = new Node(NARRD);
		narrd->record(m_symbolManager.addRecord(id->record()));
		narrd->type(m_symbolManager.addConstant(constant->record()));
		return narrd;

	} else {
		// NSIMD
		Node* nsimd = new Node(NSIMD);
		id->record()->setType(symboltable::TYPE_FLOAT);
		nsimd->record(m_symbolManager.addRecord(id->record()));
		return nsimd;
	}
}

Node* Parser::subs() {
	Node* nsubs = new Node(NSUBS);
	if(!synchronizedAdd(nsubs, std::tr1::bind(&Parser::sub, this), {scanner::TRBRC}, {scanner::TMAIN})) {
		return 0;
	}

	if(maybe(scanner::TPROC) || maybe(scanner::TFUNC)) {
		nsubs->addChild(subs());
	}

	return nsubs;
}

Node* Parser::sub() {
	if(expect(scanner::TPROC)) {
		return proc();
	} else if(expect(scanner::TFUNC)) {
		return func();
	} else {
		return 0;
	}
}

Node* Parser::proc() {
	// TODO: Procs
	debugMsg("Doing proc");
	scanner::Token* id = require(scanner::TID);

	id->record()->setType(symboltable::TYPE_PROCEDURE);

	Node* nproc = new Node(NPROC);
	nproc->record(m_symbolManager.addRecord(id->record()));

	m_symbolManager.pushScope(id->record()->getID());

	if(expect(scanner::TLPAR)) {
		nproc->addChild(plist());
		require(scanner::TRPAR);

		if(expect(scanner::TSEMI)) {
			// Declaration not definition
			return nproc;
		}
	}

	require(scanner::TLBRC);

	if(!synchronizedAdd(nproc, std::tr1::bind(&Parser::locals, this), {}, {	scanner::TID, scanner::TEXIT,
			scanner::TDISP, scanner::TNEWL,
			scanner::TCALL, scanner::TEXPT,
			scanner::TREAD})) {
	}

	enforceMaximum(scanner::TEXPT, 0, id, "procedure {{var}} contains an export statement (procedures can't have export statements)");
		nproc->addChild(stats());
	endEnforcement(scanner::TEXPT);

	require(scanner::TRBRC);

	m_symbolManager.popScope();

	return nproc;
}

Node* Parser::func() {
	debugMsg("Doing func");
	scanner::Token* id = require(scanner::TID);
	id->record()->setType(symboltable::TYPE_FUNCTION);


	require(scanner::TLPAR);

	Node* nfunc = new Node(NFUNC);
	nfunc->record(m_symbolManager.addRecord(id->record()));

	m_symbolManager.pushScope(id->record()->getID());
	nfunc->addChild(plist());

	require(scanner::TRPAR);
	require(scanner::TLBRC);
	// locals
	if(!synchronizedAdd(nfunc, std::tr1::bind(&Parser::locals, this), {}, {	scanner::TID, scanner::TEXIT,
			scanner::TDISP, scanner::TNEWL,
			scanner::TCALL, scanner::TEXPT,
			scanner::TREAD})) {
	}

	enforceMinimum(scanner::TEXPT, 1, id, "function {{var}} requires at least one export statement");
		nfunc->addChild(stats());
	endEnforcement(scanner::TEXPT);

	require(scanner::TRBRC);

	m_symbolManager.popScope();

	debugMsg("Returning func");
	return nfunc;
}

Node* Parser::plist() {
	if(maybe(scanner::TSTAR) || maybe(scanner::TID)) {
		return params();
	}

	return 0;
}

Node* Parser::params() {
	Node* nplist = new Node(NPLIST);
	if(!synchronizedAdd(nplist, std::tr1::bind(&Parser::param, this), {scanner::TCOMA}, {scanner::TRPAR})) {
		return 0;
	}

	// Paramstail
	if(expect(scanner::TCOMA)) {
		nplist->addChild(params());
	}

	return nplist;
}

Node* Parser::param() {
	// NREFP params
	if(expect(scanner::TSTAR)) {
		Node* nrefp = new Node(NREFP);
		scanner::Token* id = require(scanner::TID);
		id->record()->setType(symboltable::TYPE_REF_PARAM);

		nrefp->record(m_symbolManager.addRecord(id->record()));
		return nrefp;
	}

	// Other param types
	scanner::Token* id = require(scanner::TID);
	if(expect(scanner::TLBRK)) {
		Node* nsomething = 0;
		if(maybe(scanner::TID)) {
			// NARRP
			nsomething = new Node(NARRP);
			scanner::Token* constant = require(scanner::TID);
			id->record()->setType(symboltable::TYPE_ARRAY_BOUNDED);
			nsomething->record(m_symbolManager.addRecord(id->record()));
			nsomething->type(constant->record());
		} else {
			// NVSIZP
			nsomething = new Node(NVSIZP);
			id->record()->setType(symboltable::TYPE_ARRAY_UNBOUNDED);
			nsomething->record(m_symbolManager.addRecord(id->record()));
		}
		require(scanner::TRBRK);
		return nsomething;
	} else {
		// NSIMP
		Node* nsimp = new Node(NSIMP);
		id->record()->setType(symboltable::TYPE_FLOAT);
		nsimp->record(m_symbolManager.addRecord(id->record()));
		return nsimp;
	}
}

Node* Parser::locals() {
	if(!expect(scanner::TLOCL)) {
		return 0;
	}

	Node* nvardecls = vardecls();
	require(scanner::TSEMI);
	return nvardecls;
}

Node* Parser::stats() {
	Node* nslist = new Node(NSLIST);

	// Lets work out if we're dealing with a bigstat or stat
	std::vector<scanner::TokenID> statids = {
			scanner::TID, scanner::TEXIT,
			scanner::TDISP, scanner::TNEWL,
			scanner::TCALL, scanner::TEXPT,
			scanner::TREAD
	};

	std::vector<scanner::TokenID> bigstatids = {
			scanner::TIF, scanner::TWITH
	};


	for(auto tokID = statids.begin(); tokID != statids.end(); ++tokID) {
		if(maybe(*tokID)) {
			// We are a stat
			if(!synchronizedAdd(nslist, std::tr1::bind(&Parser::stat, this), {scanner::TSEMI}, {scanner::TEND})) {
				return 0;
			}
			nslist->addChild(stats());
			return nslist;
		}
	}

	for(auto tokID = bigstatids.begin(); tokID != bigstatids.end(); ++tokID) {
		if(maybe(*tokID)) {
			// We are a bigstat
			if(!synchronizedAdd(nslist, std::tr1::bind(&Parser::bigstat, this), {scanner::TRBRC}, {scanner::TEND})) {
				return 0;
			}
			nslist->addChild(stats());
			return nslist;
		}
	}

	return 0;
}

Node* Parser::bigstat() {
	Node* bigstat = 0;
	if(expect(scanner::TWITH)) {
		bigstat = loopstat();
	} else if(expect(scanner::TIF)) {
		bigstat = ifstat();
	}
 
	return bigstat;
}

Node* Parser::loopstat() {
	Node* nloop = new Node(NLOOP);
	debugMsg("Doing loopstat");
	nloop->addChild(asgnlist());
	debugMsg("Done asgnlist");
	require(scanner::TLOOP);

	scanner::Token* id = require(scanner::TID);
	nloop->record(id->record());

	require(scanner::TLBRC);
	enforceMinimum(scanner::TEXIT, 1, id, "loop {{var}} must contain at least one exit statement");
		nloop->addChild(stats());
	endEnforcement(scanner::TEXIT);
	require(scanner::TRBRC);

	return nloop;
}

Node* Parser::ifstat() {
	Node* nif = 0;
	Node* boolNode = boolstat();
	require(scanner::TLBRC);
	Node* nstats = stats();
	require(scanner::TRBRC);

	if(maybe(scanner::TELSE)) {
		nif = new Node(NIFTE);
		nif->addChild(boolNode);
		nif->addChild(nstats);
		require(scanner::TELSE);
		require(scanner::TLBRC);
		Node* elsestats = stats();
		nif->addChild(elsestats);
		require(scanner::TRBRC);
	} else {
		nif = new Node(NIFT);
		nif->addChild(boolNode);
		nif->addChild(nstats);
	}

	return nif;
}

Node* Parser::asgnlist() {
	Node* nalist = new Node(NALIST);
	if(!synchronizedAdd(nalist, std::tr1::bind(&Parser::asgnstat, this), {scanner::TCOMA}, {scanner::TLOOP})) {
		return 0;
	}

	//initlisttail
	if(expect(scanner::TCOMA)) {
		nalist->addChild(asgnlist());
	}

	return nalist;
}

Node* Parser::stat() {
	Node* nstat = 0;
	if(expect(scanner::TEXIT)) {
		nstat = exitstat();
	} else if(maybe(scanner::TID)) {
		nstat = asgnstat();
	} else if(maybe(scanner::TDISP)) {
		nstat = iostat();
	} else if(maybe(scanner::TNEWL)) {
		nstat = iostat();
	} else if(maybe(scanner::TREAD)) {
		nstat = iostat();
	} else if(expect(scanner::TCALL)) {
		nstat = callstat();
	} else if(expect(scanner::TEXPT)) {
		nstat = exportstat();
	}

	require(scanner::TSEMI);

	return nstat;
}

Node* Parser::exitstat() {
	scanner::Token* id = require(scanner::TID);
	require(scanner::TWHEN);

	Node* nexit = new Node(NEXIT);
	nexit->record(id->record());
	nexit->addChild(boolstat());
	return nexit;
}

Node* Parser::asgnstat() {
	Node* nasgn = new Node(NASGN);

	Node* nvar = var();
	require(scanner::TASGN);
	Node* nexpr = expr();

	// TODO: SEMANTIC CHECK
	// check if nvar can be assigned the result of nexpr

	nasgn->addChild(nvar);
	nasgn->addChild(nexpr);
	return nasgn;
}

Node* Parser::iostat() {
	if(expect(scanner::TNEWL)) {
		return new Node(NNEWLN);
	} else if(expect(scanner::TDISP)) {
		Node* ndisp = new Node(NDISP);
		ndisp->addChild(prlist());
		return ndisp;
	} else if(expect(scanner::TREAD)) {
		Node* nread = new Node(NREAD);
		nread->addChild(vlist());
		return nread;
	} else {
		return 0;
	}
}

Node* Parser::callstat() {
	scanner::Token* id = require(scanner::TID);
	require(scanner::TLPAR);

	Node* ncall = new Node(NCALL);
	ncall->record(semantic(id, {&isCallable}, "expected function or procedure found {{TYPE}}"));
	ncall->addChild(elist());
	require(scanner::TRPAR);
	return ncall;
}

Node* Parser::exportstat() {
	Node* nexpt = new Node(NEXPT);
	nexpt->addChild(expr());

	return nexpt;
}

Node* Parser::vlist() {
	Node* nvlist = new Node(NVLIST);
	if(!synchronizedAdd(nvlist, std::tr1::bind(&Parser::var, this), {scanner::TCOMA}, {scanner::TSEMI})) {
		return 0;
	}

	//initlisttail
	if(expect(scanner::TCOMA)) {
		nvlist->addChild(vlist());
	}

	debugMsg("Returning nvlist");
	return nvlist;
}

Node* Parser::var() {
	debugMsg("Doing var");
	scanner::Token* tok = require(scanner::TID);

	return varimpl(tok);

	// SEMANTIC CHECK:
	// Make sure the token exists

	// Vartail
	/*if(expect(scanner::TLBRK)) {
		Node* narrv = new Node(NARRV);

		// SEMANTIC CHECK: Make sure the token exists an is an array
		symboltable::Record* record = semantic(tok->record(), {&isArray});
		narrv->record(record);

		narrv->addChild(expr());
		require(scanner::TRBRK);
		return narrv;
	} else {
		Node* nsimv = new Node(NSIMV);
		symboltable::Record* record = semantic(tok->record(), {&isSimple});
		nsimv->record(record);
		return nsimv;
	}*/
}

Node* Parser::varimpl(scanner::Token* tok) {
	if(expect(scanner::TLBRK)) {
		Node* narrv = new Node(NARRV);

		// SEMANTIC CHECK: Make sure the token exists an is an array
		symboltable::Record* record = semantic(tok, {&isArray}, "expected array found {{type}}");
		narrv->record(record);

		narrv->addChild(expr());
		require(scanner::TRBRK);
		return narrv;
	} else {
		Node* nsimv = new Node(NSIMV);
		symboltable::Record* record = semantic(tok, {&isSimple}, "expected simple found {{type}}");
		nsimv->record(record);
		return nsimv;
	}
}

Node* Parser::prlist() {
	Node* nprlist = new Node(NPRLST);
	if(!synchronizedAdd(nprlist, std::tr1::bind(&Parser::printitem, this), {scanner::TCOMA}, {scanner::TSEMI})) {
		return 0;
	}

	//initlisttail
	if(expect(scanner::TCOMA)) {
		nprlist->addChild(prlist());
	}

	return nprlist;
}

Node* Parser::printitem() {
	if(maybe(scanner::TSTR)) {
		// String
		scanner::Token* str = require(scanner::TSTR);
		Node* nstrg = new Node(NSTRG);
		nstrg->record(str->record());
		return nstrg;
	} else {
		return expr();
	}
}

Node* Parser::elist() {
	// Cheat and check if the next token is a ), if so return
	if(maybe(scanner::TRPAR)) {
		return 0;
	}

	Node* nelist = new Node(NELIST);
	if(!synchronizedAdd(nelist, std::tr1::bind(&Parser::printitem, this), {scanner::TCOMA}, {scanner::TSEMI})) {
		return 0;
	}

	//initlisttail
	if(expect(scanner::TCOMA)) {
		nelist->addChild(elist());
	}

	return nelist;
}

Node* Parser::boolstat() {
	Node* nrel = rel();
	Node* nbtail = booltail(nrel);
	return nbtail;
}

Node* Parser::booltail(Node* subtree) {
	if(maybe(scanner::TAND) || maybe(scanner::TOR)) {
		Node* nlogop = logop();
		nlogop->addChild(subtree);
		nlogop->addChild(rel());
		return booltail(nlogop);
	}

	return subtree;
}

Node* Parser::rel() {
	if(expect(scanner::TNOT)) {
		Node* nnot = new Node(NNOT);
		Node* nexpr = expr();
		Node* nreltail = reltail(nexpr);
		nnot->addChild(nreltail);
		return nnot;
	}

	Node* nexpr = expr();
	Node* nreltail = reltail(nexpr);
	return nreltail;
}

Node* Parser::reltail(Node* subtree) {
	if(maybe(scanner::TEEQ) || maybe(scanner::TNEE)
		|| maybe(scanner::TGT) || maybe(scanner::TGE)
		|| maybe(scanner::TLT) || maybe(scanner::TLE)) {
		Node* nrelop = relop();
		Node* nexpr = expr();
		nrelop->addChild(subtree);
		nrelop->addChild(nexpr);
		return reltail(nrelop);
	}

	return subtree;
}

Node* Parser::logop() {
	if(expect(scanner::TAND)) {
		return new Node(NAND);
	} else if(expect(scanner::TOR)) {
		return new Node(NOR);
	} else {
		return 0;
	}
}

Node* Parser::relop() {
	std::map<scanner::TokenID, NodeID> ids;
	ids[scanner::TEEQ] = NAEQ;
	ids[scanner::TNEE] = NNEQ;
	ids[scanner::TGT] = NGTR;
	ids[scanner::TGE] = NGEQ;
	ids[scanner::TLT] = NLSS;
	ids[scanner::TLE] = NLEQ;

	for(auto pair = ids.begin(); pair != ids.end(); ++pair) {
		if(expect(pair->first)) {
			Node* nop = new Node(pair->second);
			return nop;
		}
	}

	return 0;
}

Node* Parser::expr() {
	Node* nterm = term();
	Node* netail = etail(nterm);
	return netail;
}

Node* Parser::etail(Node* subtree) {
	Node* tailnode = 0;
	if(expect(scanner::TPLUS)) {
		tailnode = new Node(NADD);
	} else if(expect(scanner::TMIN)) {
		tailnode = new Node(NSUB);
	} else {
		return subtree;
	}

	tailnode->addChild(subtree);
	tailnode->addChild(term());
	return etail(tailnode);
}

Node* Parser::term() {
	Node* nfact = fact();
	Node* nttail = ttail(nfact);
	return nttail;
}

Node* Parser::ttail(Node* subtree) {
	Node* tailnode = 0;
	if(expect(scanner::TSTAR)) {
		tailnode = new Node(NMUL);
	} else if(expect(scanner::TDIV)) {
		tailnode = new Node(NDIV);
	} else {
		return subtree;
	}

	tailnode->addChild(subtree);
	tailnode->addChild(fact());
	return ttail(tailnode);
}

Node* Parser::fact() {
	Node* nexponent = exponent();
	Node* nftail = ftail(nexponent);
	return nftail;
}

Node* Parser::ftail(Node* subtree) {
	Node* tailnode = 0;
	if(expect(scanner::TUPAR)) {
		tailnode = new Node(NPOW);
	} else {
		return subtree;
	}

	tailnode->addChild(subtree);
	tailnode->addChild(exponent());
	return ftail(tailnode);
}

Node* Parser::exponent() {
	// Check for non-id based expressions reallit, ( <bool> )
	if(maybe(scanner::TFLIT)) {
		Node* nflit = new Node(NFLIT);
		scanner::Token* flitid = require(scanner::TFLIT);
		nflit->record(flitid->record());
		return nflit;
	} else if(expect(scanner::TLPAR)) { // ( <bool> )
		Node* nbool = boolstat();
		require(scanner::TRPAR);
		return nbool;
	}

	// Id based exponent possibilites
	scanner::Token* id = require(scanner::TID);
	if(expect(scanner::TDOT)) {
		require(scanner::TLENG);
		Node* nleng = new Node(NLENG);

		// SEMANTIC CHECKING:

		// If we're here then id needs to have already been declared
		symboltable::Record* look = semantic(id, {&isArray}, "attempt to use .length on non-array variable {{var}} (type is {{type}})");

		// Assoicate the symbol table record
		nleng->record(look);

		return nleng;
	} else if(expect(scanner::TLPAR)) { // Assume fncall
		Node* nfncl = new Node(NFNCL);

		nfncl->record(semantic(id, {&isFunction}, "attempt to use variable {{var}} as function"));
		//nfncl->record(id->record());
		nfncl->addChild(fncalltail());
		return nfncl;
	} else { // assume <var>
		return varimpl(id);
	}
}

Node* Parser::fncalltail() {
	Node* nelist = elist();
	require(scanner::TRPAR);
	return nelist;
}
