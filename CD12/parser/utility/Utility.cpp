/*
 * Utility.cpp
 *
 *  Created on: Sep 12, 2012
 *      Author: vodurden
 */

#include "Utility.h"

namespace cd12 {
namespace parser {

const char* Utility::NODE_ID_NAMES[] = {
	"NPROG",
	"NGDECS",
	"NSUBS",
	"NCDECS",
	"NCONST",
	"NVDECS",
	"NARRD",
	"NSIMD",
	"NFUNC",
	"NPROC",
	"NPLIST",
	"NREFP",
	"NARRP",
	"NVSIZP",
	"NSIMP",
	"NSLIST",
	"NLOOP",
	"NALIST",
	"NIFTE",
	"NIFT",
	"NEXIT",
	"NASGN",
	"NREAD",
	"NDISP",
	"NNEWLN",
	"NCALL",
	"NEXPT",
	"NVLIST",
	"NARRV",
	"NSIMV",
	"NELIST",
	"NNOT",
	"NADD",
	"NSUB",
	"NMUL",
	"NDIV",
	"NPOW",
	"NFLIT",
	"NLENG",
	"NFNCL",
	"NPRLST",
	"NSTRG",
	"NAND",
	"NOR",
	"NAEQ",
	"NNEQ",
	"NGTR",
	"NLEQ",
	"NLSS",
	"NGEQ",
};

std::string Utility::idToString(NodeID id) {
	return NODE_ID_NAMES[id-1];
}

}
}
