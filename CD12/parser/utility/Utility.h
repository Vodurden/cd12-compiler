/*
 * Utility.h
 *
 *  Created on: Sep 12, 2012
 *      Author: vodurden
 */

#ifndef CD12_PARSER_UTILITY_H
#define CD12_PARSER_UTILITY_H
#include <string>
#include "../NodeID.h"
namespace cd12 {
namespace parser {

	class Utility {
		public:
			static std::string idToString(NodeID id);
		private:
			static const char* NODE_ID_NAMES[];
	};

}
}
#endif
