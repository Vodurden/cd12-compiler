/*
 * TokenExaminer.cpp
 *
 *  Created on: Sep 9, 2012
 *      Author: vodurden
 */

#include "TokenExaminer.h"

namespace cd12 {
namespace parser {

TokenExaminer::TokenExaminer(scanner::IScanner& scanner) :
	m_scanner(scanner),
	m_current(0),
	m_next(0) {
}

TokenExaminer::~TokenExaminer() {

}

scanner::Token* TokenExaminer::next() {
	if(m_next == 0 && m_current == 0) {
		// First read do setup
		m_current = m_scanner.parseNext();
		m_next = m_scanner.parseNext();

		return m_current;
	}

	// Handle the end case
	if(m_next == 0) {
		m_current = 0;
		return 0;
	}

	// Handle the 2nd last token case
	if(!m_scanner.canParse()) {
		m_current = m_next;
		m_next = 0;
		return m_current;
	}

	m_current = m_next;
	m_next = m_scanner.parseNext();
	return m_current;
}

scanner::Token* TokenExaminer::get() {
	return m_current;
}

scanner::Token* TokenExaminer::peek() {
	if(m_next == 0 && m_current == 0) {
		m_next = m_scanner.parseNext();
	}

	return m_next;
}

bool TokenExaminer::hasNext() {
	return (m_scanner.canParse()) || (m_next != 0);
}

}
}
