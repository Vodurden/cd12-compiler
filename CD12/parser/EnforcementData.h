#ifndef CD12_PARSER_ENFORCEMENTDATA_H
#define CD12_PARSER_ENFORCEMENTDATA_H
#include <string>
namespace cd12 {
	namespace parser {
		struct EnforcementData  {
			EnforcementData() :
				count(0),
				min(0),
				scope(0)
				{

			}

			EnforcementData(int c, int minmax, scanner::Token* s, const std::string& message) :
				count(c),
				min(minmax),
				scope(s),
				errorMessage(message) {
			}

			int count;
			union {
				int min;
				int max;
			};

			scanner::Token* scope;
			std::string errorMessage;
		};
	}
}
#endif
